package br.senai.sp.helpdesk.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;

import br.senai.sp.helpdesk.dao.DaoSol;
import br.senai.sp.helpdesk.modelo.Solicitante;
import br.senai.sp.helpdesk.tables.TableModelSolicitante;

public class JanelaSolicitante extends JInternalFrame {
	private DaoSol dao;
	private Color corPadrao;
	private Color corFonte;
	private Font fontePadrao;
	private JLabel lbNome;
	private JTextField tfNome;
	private JButton btSalvar;
	private JButton btLimpar;
	private JButton btExcluir;
	private JTable tbUsuarios;
	private TableModelSolicitante model;
	private Solicitante solicitante;
	private JLabel lbEmail;
	private JTextField tfEmail;
	private JScrollPane spSolicitantes;
	private JLabel lbUser;
	private JTextField tfUser;
	private Component lbSenha;
	private JTextField tfSenha;
	private JTextField tfBuscar;
	private AbstractButton btBuscar;

	public JanelaSolicitante() {
		inicializarComponentes();
		definirEventos();
	}

	private void inicializarComponentes() {
		try {
			dao = new DaoSol();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, "Erro: " + e1.getMessage(),
					"Erro", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();

		}

		Set<KeyStroke> teclas = new HashSet<KeyStroke>();
		teclas.add(KeyStroke.getKeyStroke("TAB"));

		corPadrao = new Color(180, 180, 180);

		corFonte = new Color(255, 255, 255);

		fontePadrao = new Font("System", Font.BOLD, 14);

		lbNome = new JLabel("Nome:");
		lbNome.setSize(80, 25);
		lbNome.setLocation(10, 10);

		tfNome = new JTextField();
		tfNome.setBounds(100, 10, 250, 25);

		lbEmail = new JLabel("E-Mail:");
		lbEmail.setSize(80, 25);
		lbEmail.setLocation(10, 45);

		tfEmail = new JTextField();
		tfEmail.setBounds(100, 45, 250, 25);

		lbUser = new JLabel("Usuario:");
		lbUser.setSize(80, 25);
		lbUser.setLocation(10, 80);

		tfUser = new JTextField();
		tfUser.setBounds(100, 80, 250, 25);

		lbSenha = new JLabel("Senha:");
		lbSenha.setSize(80, 25);
		lbSenha.setLocation(10, 115);

		tfSenha = new JTextField();
		tfSenha.setBounds(100, 115, 250, 25);

		btSalvar = new JButton("Salvar");
		btSalvar.setFont(fontePadrao);

		btLimpar = new JButton("Limpar");
		btLimpar.setFont(fontePadrao);

		btExcluir = new JButton("Excluir");
		btExcluir.setFont(fontePadrao);

		JPanel pnBotoes = new JPanel(new GridLayout(1, 3));
		pnBotoes.add(btSalvar);
		pnBotoes.add(btExcluir);
		pnBotoes.add(btLimpar);
		pnBotoes.setBounds(0, 150, 370, 25);
		pnBotoes.setBackground(Color.black);

		tfBuscar = new JTextField();
		tfBuscar.setBounds(0, 185, 340, 25);
		tfBuscar.setFont(fontePadrao);

		btBuscar = new JButton();
		btBuscar.setIcon(new ImageIcon(getClass().getResource(
				"/Imagens/search.png")));

		JPanel pnBuscar = new JPanel(new GridLayout(1, 1));
		pnBuscar.add(btBuscar);
		pnBuscar.setBounds(345, 185, 25, 25);
		pnBuscar.setBackground(Color.black);

		tbUsuarios = new JTable();

		spSolicitantes = new JScrollPane(tbUsuarios);
		spSolicitantes.setBounds(0, 220, 370, 220);
		spSolicitantes
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		// parametros do frame
		// setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(380, 470);
		setTitle("SOLICITANTES");
		setClosable(true);
		setMaximizable(false);
		setLocation(380, 200);
		setResizable(false);
		getContentPane().setBackground(corPadrao);
		getRootPane().setDefaultButton(btSalvar);
		setContentPane(new Fundo());
		// setFrameIcon(new
		// ImageIcon(getClass().getResource("/Imagens/icon_Aluno.png")));

		try {
			criartabela(dao.listar());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Erro ao listar Tecnicos",
					"Erro", JOptionPane.ERROR_MESSAGE);
		}

		// ------------------------------------------------------------//
		setLayout(null);
		add(lbNome);
		add(tfNome);
		add(lbEmail);
		add(tfEmail);
		add(lbUser);
		add(tfUser);
		add(lbSenha);
		add(tfSenha);
		add(spSolicitantes);
		add(pnBuscar);
		add(tfBuscar);
		add(pnBotoes);
		add(lbEmail);
		add(tfEmail);

		setVisible(true);
		for (Component c : getContentPane().getComponents()) {
			c.setFont(fontePadrao);
			if (c instanceof JLabel) {
				c.setForeground(corFonte);
			}
		}

	}

	private class Fundo extends JPanel {
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D graph = (Graphics2D) g;
			Image imagem = new ImageIcon(getClass().getResource(
					"/Imagens/D-edge.jpg")).getImage();
			graph.drawImage(imagem, 0, 0, this.getWidth(), this.getHeight(),
					this);
		}
	}

	private void definirEventos() {
		btBuscar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!tfBuscar.getText().trim().isEmpty()) {
					try {
						criartabela(dao.buscar(tfBuscar.getText().toString()));
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(JanelaSolicitante.this,
								"Erro ao buscar", "Erro",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btLimpar.addActionListener(e -> {
			limpar();
		});

		btExcluir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (solicitante != null) {
					if (JOptionPane.showConfirmDialog(
							JanelaSolicitante.this,
							"Deseja realmente excluir o usuario "
									+ solicitante.getNome() + "?",
							"Confirmar exclus�o", JOptionPane.YES_NO_OPTION) == 0) {
						try {
							dao.excluir(solicitante.getId());
							criartabela(dao.listar());
							limpar();
						} catch (Exception e2) {
							JOptionPane.showMessageDialog(
									JanelaSolicitante.this, e2.getMessage());
						}
					}
				}

			}
		});

		tbUsuarios.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent e) {
						int linhaSelecionada = tbUsuarios.getSelectedRow();
						if (linhaSelecionada >= 0) {
							solicitante = model
									.getSolicitante(linhaSelecionada);
							tfNome.setText(solicitante.getNome());
							tfEmail.setText(solicitante.getEmail());
							tfUser.setText(solicitante.getUsuario());
							tfSenha.setText(solicitante.getSenha());
						}

					}
				});

		btSalvar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (tfNome.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(JanelaSolicitante.this,
							"Nome inv�lido", "Erro", JOptionPane.ERROR_MESSAGE);
					tfNome.requestFocus();
				} else if (tfEmail.getText().trim().isEmpty()) {
					JOptionPane
							.showMessageDialog(JanelaSolicitante.this,
									"Email inv�lido", "Erro",
									JOptionPane.ERROR_MESSAGE);
					tfEmail.requestFocus();
				} else if (tfUser.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(JanelaSolicitante.this,
							"Usu�rio inv�lido", "Erro",
							JOptionPane.ERROR_MESSAGE);
					tfUser.requestFocus();
				} else if (tfSenha.getText().trim().isEmpty()) {
					JOptionPane
							.showMessageDialog(JanelaSolicitante.this,
									"Senha inv�lida", "Erro",
									JOptionPane.ERROR_MESSAGE);
					tfSenha.requestFocus();
				} else {
					if (solicitante == null) {
						solicitante = new Solicitante();
						solicitante.setNome(tfNome.getText());
						solicitante.setEmail(tfEmail.getText());
						solicitante.setUsuario(tfUser.getText());
						solicitante.setSenha(tfSenha.getText());
						try {
							dao.inserir(solicitante);
							limpar();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(
									JanelaSolicitante.this, "Erro ao salvar:"
											+ e1.getMessage(), "Erro",
									JOptionPane.ERROR_MESSAGE);
						}
					} else {
						solicitante.setNome(tfNome.getText());
						solicitante.setEmail(tfEmail.getText());
						solicitante.setUsuario(tfUser.getText());
						solicitante.setSenha(tfSenha.getText());
						try {
							dao.alterar(solicitante);
							limpar();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(
									JanelaSolicitante.this, "Erro ao alterar:"
											+ e1.getMessage(), "Erro",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				try {
					criartabela(dao.listar());
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(JanelaSolicitante.this,
							"Erro: " + e1.getMessage(), "Erro",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

	}

	private void criartabela(List<Solicitante> solicitantes) {
		model = new TableModelSolicitante(solicitantes);
		tbUsuarios.setModel(model);
		tbUsuarios.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tbUsuarios.setSelectionBackground(Color.BLACK);
		tbUsuarios.getColumnModel().getColumn(0).setPreferredWidth(70);
		tbUsuarios.getColumnModel().getColumn(1).setPreferredWidth(150);
		tbUsuarios.getColumnModel().getColumn(2).setPreferredWidth(64);
		tbUsuarios.getColumnModel().getColumn(3).setPreferredWidth(64);
		tbUsuarios.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tbUsuarios.setRowHeight(25);
		tbUsuarios.getTableHeader().setReorderingAllowed(false);
		tbUsuarios.getTableHeader().setResizingAllowed(false);

		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		tbUsuarios.getColumnModel().getColumn(0).setCellRenderer(render);
	}

	private void limpar() {
		solicitante = null;
		tfNome.setText(null);
		tfEmail.setText(null);
		tfUser.setText(null);
		tfSenha.setText(null);
	}

}

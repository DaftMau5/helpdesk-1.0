package br.senai.sp.helpdesk.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.TextArea;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;

public class JanelaSobre extends JInternalFrame {

	private Color corPadrao;
	private Color corFonte;
	private Font fontePadrao;
	private TextArea taSobre;

	public JanelaSobre() {
		inicializarComponentes();
		definirEventos();
	}

	private void inicializarComponentes() {
		corPadrao = new Color(0, 0, 0);

		corFonte = new Color(0, 255, 0);

		fontePadrao = new Font("System", Font.ITALIC, 12);

		taSobre = new TextArea();
		taSobre.setBounds(0, 0, 290, 290);
		taSobre.setText("****NeonLine Technology Services****\n" + "Help Desk\n" + " - Eduardo Carvalho\n"
				+ " - Felipe Silva\n" + "T�cnico em Inform�tica\n" + "Segundo m�dulo\n" + "SENAI de Inform�tica\n"
				+ "Dezembro, 2015\n" + "-----------------------------------\n"
						+ "");
		taSobre.setEditable(false);
		taSobre.setBackground(Color.black);
		taSobre.setForeground(Color.cyan);

		// parametros do frame
		setSize(300, 300);
		setTitle("SOBRE N�S");
		setClosable(true);
		setMaximizable(false);
		setResizable(false);
		getContentPane().setBackground(corPadrao);

		setLayout(null);
		add(taSobre);
		setVisible(true);
		for (Component c : getContentPane().getComponents()) {
			c.setFont(fontePadrao);
			if (c instanceof JLabel) {
				c.setForeground(corFonte);
			}
		}

	}

	private void definirEventos() {

	}
}

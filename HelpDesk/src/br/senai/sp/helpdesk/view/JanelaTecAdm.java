package br.senai.sp.helpdesk.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;

import br.senai.sp.helpdesk.dao.DaoTec;
import br.senai.sp.helpdesk.modelo.Tecnico;
import br.senai.sp.helpdesk.modelo.Tipo;
import br.senai.sp.helpdesk.tables.TableModelTecnico;

public class JanelaTecAdm extends JInternalFrame {
	private DaoTec dao;
	private Color corPadrao;
	private Color corFonte;
	private Font fontePadrao;
	private JLabel lbNome;
	private JTextField tfNome;
	private JComboBox<Tipo> cbTipo;
	private Component lbTipo;
	private JButton btSalvar;
	private JButton btLimpar;
	private JButton btExcluir;
	private JLabel lbEquipamentos;
	private JTable tbTecnicos;
	private JScrollPane spEquipamentos;
	private TableModelTecnico model;
	private Tecnico tecnico;
	private JLabel lbEmail;
	private JTextField tfEmail;
	private JScrollPane spTecnicos;
	private JLabel lbTecnicos;
	private JLabel lbUser;
	private JTextField tfUser;
	private JLabel lbSenha;
	private JTextField tfSenha;
	private Tecnico s;
	private JButton btBuscar;
	private JTextField tfBuscar;

	public JanelaTecAdm() {
		inicializarComponentes();
		definirEventos();
	}

	private void inicializarComponentes() {
		try {
			dao = new DaoTec();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, "Erro: " + e1.getMessage(),
					"Erro", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();

		}

		Set<KeyStroke> teclas = new HashSet<KeyStroke>();
		teclas.add(KeyStroke.getKeyStroke("TAB"));

		corPadrao = new Color(180, 180, 180);

		corFonte = new Color(255, 255, 255);

		fontePadrao = new Font("System", Font.BOLD, 14);

		lbNome = new JLabel("Nome:");
		lbNome.setSize(80, 25);
		lbNome.setLocation(10, 10);

		tfNome = new JTextField();
		tfNome.setBounds(100, 10, 250, 25);

		lbTipo = new JLabel("Tipo:");
		lbTipo.setSize(80, 25);
		lbTipo.setLocation(10, 45);

		cbTipo = new JComboBox<Tipo>(Tipo.values());
		cbTipo.setBounds(100, 45, 250, 25);
		cbTipo.setSelectedIndex(-1);

		lbEmail = new JLabel("E-Mail:");
		lbEmail.setSize(80, 25);
		lbEmail.setLocation(10, 80);

		tfEmail = new JTextField();
		tfEmail.setBounds(100, 80, 250, 25);

		lbUser = new JLabel("Usuario:");
		lbUser.setSize(80, 25);
		lbUser.setLocation(10, 115);

		tfUser = new JTextField();
		tfUser.setBounds(100, 115, 250, 25);

		lbSenha = new JLabel("Senha:");
		lbSenha.setSize(80, 25);
		lbSenha.setLocation(10, 150);

		tfSenha = new JTextField();
		tfSenha.setBounds(100, 150, 250, 25);

		btSalvar = new JButton("Salvar");
		btSalvar.setFont(fontePadrao);

		btLimpar = new JButton("Limpar");
		btLimpar.setFont(fontePadrao);

		btExcluir = new JButton("Excluir");
		btExcluir.setFont(fontePadrao);

		JPanel pnBotoes = new JPanel(new GridLayout(1, 3));
		pnBotoes.add(btSalvar);
		pnBotoes.add(btExcluir);
		pnBotoes.add(btLimpar);
		pnBotoes.setBounds(0, 185, 370, 25);

		tbTecnicos = new JTable();

		spTecnicos = new JScrollPane(tbTecnicos);
		spTecnicos.setBounds(0, 260, 370, 220);
		spTecnicos
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		tfBuscar = new JTextField();
		tfBuscar.setBounds(0, 220, 340, 25);
		tfBuscar.setFont(fontePadrao);

		btBuscar = new JButton();
		btBuscar.setIcon(new ImageIcon(getClass().getResource(
				"/Imagens/search.png")));

		JPanel pnBuscar = new JPanel(new GridLayout(1, 1));
		pnBuscar.add(btBuscar);
		pnBuscar.setBounds(345, 220, 25, 25);
		pnBotoes.setBackground(Color.black);
		pnBuscar.setBackground(Color.black);

		// parametros do frame
		// setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(382, 510);
		setTitle("TECNICOS");
		setClosable(true);
		setMaximizable(false);
		setLocation(410, 200);
		setResizable(false);
		getContentPane().setBackground(corPadrao);
		getRootPane().setDefaultButton(btSalvar);
		setContentPane(new Fundo());
		// setFrameIcon(new
		// ImageIcon(getClass().getResource("/Imagens/icon_Aluno.png")));

		try {
			criartabela(dao.listar());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Erro ao listar Tecnicos",
					"Erro", JOptionPane.ERROR_MESSAGE);
		}

		// ------------------------------------------------------------//
		setLayout(null);
		add(lbNome);
		add(tfNome);
		add(lbTipo);
		add(cbTipo);
		add(spTecnicos);
		add(pnBotoes);
		add(lbEmail);
		add(tfEmail);
		add(lbUser);
		add(tfUser);
		add(lbSenha);
		add(tfSenha);
		add(tfBuscar);
		add(pnBuscar);

		setVisible(true);
		for (Component c : getContentPane().getComponents()) {
			c.setFont(fontePadrao);
			if (c instanceof JLabel) {
				c.setForeground(corFonte);
			}
		}

	}

	private class Fundo extends JDesktopPane {
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D graph = (Graphics2D) g;
			Image imagem = new ImageIcon(getClass().getResource(
					"/Imagens/D-edge.jpg")).getImage();
			graph.drawImage(imagem, 0, 0, this.getWidth(), this.getHeight(),
					this);
		}
	}

	private void definirEventos() {
		btBuscar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!tfBuscar.getText().trim().isEmpty()) {
					try {
						criartabela(dao.buscar(tfBuscar.getText().toString()));
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(JanelaTecAdm.this,
								"Erro ao buscar", "Erro",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btLimpar.addActionListener(e -> {
			limpar();
		});

		btExcluir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (tecnico != null) {
					if (JOptionPane.showConfirmDialog(
							JanelaTecAdm.this,
							"Deseja realmente excluir o tecnico "
									+ tecnico.getNome() + "?",
							"Confirmar exclus�o", JOptionPane.YES_NO_OPTION) == 0) {
						try {
							dao.excluir(tecnico.getId());
							criartabela(dao.listar());
							limpar();
						} catch (Exception e2) {
							JOptionPane.showMessageDialog(JanelaTecAdm.this,
									e2.getMessage());
						}
					}
				}

			}
		});

		tbTecnicos.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent e) {
						int linhaSelecionada = tbTecnicos.getSelectedRow();
						if (linhaSelecionada >= 0) {
							tecnico = model.getTecnico(linhaSelecionada);
							tfNome.setText(tecnico.getNome());
							cbTipo.setSelectedItem(tecnico.getTipo());
							tfEmail.setText(tecnico.getEmail());
							tfUser.setText(tecnico.getUsuario());
							tfSenha.setText(tecnico.getSenha());
						}

					}
				});

		btSalvar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (tfNome.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(JanelaTecAdm.this,
							"Nome inv�lido", "Erro", JOptionPane.ERROR_MESSAGE);
					tfNome.requestFocus();
				} else if (cbTipo.getSelectedIndex() < 0) {
					JOptionPane
							.showMessageDialog(JanelaTecAdm.this,
									"Informe o tipo", "Erro",
									JOptionPane.ERROR_MESSAGE);
				} else if (tfEmail.getText().trim().isEmpty()) {
					JOptionPane
							.showMessageDialog(JanelaTecAdm.this,
									"Email inv�lido", "Erro",
									JOptionPane.ERROR_MESSAGE);
					tfEmail.requestFocus();
				} else if (tfUser.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(JanelaTecAdm.this,
							"Usu�rio inv�lido", "Erro",
							JOptionPane.ERROR_MESSAGE);
					tfUser.requestFocus();
				} else if (tfSenha.getText().trim().isEmpty()) {
					JOptionPane
							.showMessageDialog(JanelaTecAdm.this,
									"Senha inv�lida", "Erro",
									JOptionPane.ERROR_MESSAGE);
					tfSenha.requestFocus();
				} else {
					if (tecnico == null) {
						tecnico = new Tecnico();
						tecnico.setNome(tfNome.getText());
						tecnico.setTipo((Tipo) cbTipo.getSelectedItem());
						tecnico.setEmail(tfEmail.getText());
						tecnico.setUsuario(tfUser.getText());
						tecnico.setSenha(tfSenha.getText());
						try {
							dao.inserir(tecnico);
							limpar();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(JanelaTecAdm.this,
									"Erro ao salvar:" + e1.getMessage(),
									"Erro", JOptionPane.ERROR_MESSAGE);
						}
					} else {
						tecnico.setNome(tfNome.getText());
						tecnico.setTipo((Tipo) cbTipo.getSelectedItem());
						tecnico.setEmail(tfEmail.getText());
						tecnico.setUsuario(tfUser.getText());
						tecnico.setSenha(tfSenha.getText());
						try {
							dao.alterar(tecnico);
							limpar();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(JanelaTecAdm.this,
									"Erro ao alterar:" + e1.getMessage(),
									"Erro", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				try {
					criartabela(dao.listar());
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(JanelaTecAdm.this, "Erro: "
							+ e1.getMessage(), "Erro",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

	}

	private void criartabela(List<Tecnico> tecnicos) {
		model = new TableModelTecnico(tecnicos);
		tbTecnicos.setModel(model);
		tbTecnicos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tbTecnicos.setSelectionBackground(Color.BLACK);
		tbTecnicos.getColumnModel().getColumn(0).setPreferredWidth(70);
		tbTecnicos.getColumnModel().getColumn(1).setPreferredWidth(150);
		tbTecnicos.getColumnModel().getColumn(2).setPreferredWidth(128);
		tbTecnicos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tbTecnicos.setRowHeight(25);
		tbTecnicos.getTableHeader().setReorderingAllowed(false);
		tbTecnicos.getTableHeader().setResizingAllowed(false);

		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		tbTecnicos.getColumnModel().getColumn(0).setCellRenderer(render);
	}

	private void limpar() {
		tecnico = null;
		cbTipo.setSelectedIndex(-1);
		tfNome.setText(null);
		tfEmail.setText(null);
		tfUser.setText(null);
		tfSenha.setText(null);
	}

}

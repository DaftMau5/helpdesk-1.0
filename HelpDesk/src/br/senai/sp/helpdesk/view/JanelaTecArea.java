package br.senai.sp.helpdesk.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;

import br.senai.sp.helpdesk.dao.DaoChamado;
import br.senai.sp.helpdesk.modelo.Chamado;
import br.senai.sp.helpdesk.modelo.Tecnico;
import br.senai.sp.helpdesk.tables.TableModelChamadoTec;

public class JanelaTecArea extends JInternalFrame {

	private Color corPadrao;
	private Color corFonte;
	private Font fontePadrao;
	private JButton btChamadoTrue;
	private JButton btChamadoFalse;
	private JTable tbChamados;
	private JScrollPane spChamados;
	private TableModelChamadoTec model;
	private DaoChamado dao;
	private Chamado chamado;
	private Tecnico s;

	public JanelaTecArea(Tecnico s) {
		this.s = s;
		inicializarComponentes();
		definirEventos();
	}

	private void inicializarComponentes() {
		try {
			dao = new DaoChamado();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, "Erro: " + e1.getMessage(),
					"Erro", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		}

		corPadrao = new Color(180, 180, 180);

		corFonte = new Color(255, 255, 255);

		fontePadrao = new Font("System", Font.BOLD, 14);

		btChamadoFalse = new JButton("Fechar chamado");
		btChamadoFalse.setFont(fontePadrao);

		btChamadoTrue = new JButton("Reabrir chamado");
		btChamadoTrue.setFont(fontePadrao);

		JPanel pnBotoes = new JPanel(new GridLayout(1, 2));
		pnBotoes.add(btChamadoTrue);
		pnBotoes.add(btChamadoFalse);
		pnBotoes.setBounds(0, 500, 1200, 50);

		pnBotoes.setBackground(Color.black);
		btChamadoFalse.setBackground(Color.black);
		btChamadoTrue.setBackground(Color.black);
		btChamadoFalse.setForeground(Color.white);
		btChamadoTrue.setForeground(Color.white);
		btChamadoFalse.setIcon(new ImageIcon(getClass().getResource("/Imagens/fecharChamado.png")));
		btChamadoTrue.setIcon(new ImageIcon(getClass().getResource("/Imagens/reabrirChamado.png")));
		
		tbChamados = new JTable();

		spChamados = new JScrollPane(tbChamados);
		spChamados.setBounds(0, 0, 1200, 490);
		spChamados
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		for (Component c : getContentPane().getComponents()) {
			c.setFont(fontePadrao);
			if (c instanceof JLabel) {
				c.setForeground(corFonte);
			}
		}

		// parametros do frame
		setSize(1210, 590);
		setTitle("AREA DO TECNICO - " + s.getNome());
		setClosable(true);
		setMaximizable(false);
		setResizable(false);
		getContentPane().setBackground(corPadrao);
		setContentPane(new Fundo());

		try {
			criartabela(dao.listarTec(s));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Erro ao listar Chamados",
					"Erro", JOptionPane.ERROR_MESSAGE);
		}

		setLayout(null);
		add(pnBotoes);
		add(spChamados);
		setVisible(true);

	}

	private class Fundo extends JDesktopPane {
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D graph = (Graphics2D) g;
			Image imagem = new ImageIcon(getClass().getResource(
					"/Imagens/D-edge.jpg")).getImage();
			graph.drawImage(imagem, 0, 0, this.getWidth(), this.getHeight(),
					this);
		}
	}

	private void definirEventos() {
		tbChamados.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent e) {
						int linhaSelecionada = tbChamados.getSelectedRow();
						if (linhaSelecionada >= 0) {
							chamado = model.getChamado(linhaSelecionada);
						}
					}
				});
		btChamadoTrue.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				chamado.setStatus(true);
				try {
					dao.alterar(chamado);
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(JanelaTecArea.this,
							"Erro ao salvar:" + e1.getMessage(), "Erro",
							JOptionPane.ERROR_MESSAGE);
				}
				try {
					criartabela(dao.listarTec(s));
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(JanelaTecArea.this, "Erro: "
							+ e1.getMessage(), "Erro",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btChamadoFalse.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				chamado.setStatus(false);
				try {
					dao.alterar(chamado);
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(JanelaTecArea.this,
							"Erro ao salvar:" + e1.getMessage(), "Erro",
							JOptionPane.ERROR_MESSAGE);
				}
				try {
					criartabela(dao.listarTec(s));
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(JanelaTecArea.this, "Erro: "
							+ e1.getMessage(), "Erro",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}

	private void criartabela(List<Chamado> chamados) {
		model = new TableModelChamadoTec(chamados);
		tbChamados.setModel(model);
		tbChamados.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tbChamados.setSelectionBackground(Color.BLACK);
		tbChamados.getColumnModel().getColumn(0).setPreferredWidth(50);
		tbChamados.getColumnModel().getColumn(1).setPreferredWidth(100);
		tbChamados.getColumnModel().getColumn(2).setPreferredWidth(100);
		tbChamados.getColumnModel().getColumn(3).setPreferredWidth(100);
		tbChamados.getColumnModel().getColumn(4).setPreferredWidth(50);
		tbChamados.getColumnModel().getColumn(5).setPreferredWidth(200);
		tbChamados.getColumnModel().getColumn(6).setPreferredWidth(200);
		tbChamados.getColumnModel().getColumn(7).setPreferredWidth(250);
		tbChamados.getColumnModel().getColumn(8).setPreferredWidth(128);
		tbChamados.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tbChamados.setRowHeight(25);
		tbChamados.getTableHeader().setReorderingAllowed(false);
		tbChamados.getTableHeader().setResizingAllowed(false);

		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		tbChamados.getColumnModel().getColumn(0).setCellRenderer(render);
	}
}

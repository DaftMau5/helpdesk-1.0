package br.senai.sp.helpdesk.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;

import br.senai.sp.helpdesk.dao.DaoEquipamento;
import br.senai.sp.helpdesk.modelo.Equipamento;
import br.senai.sp.helpdesk.modelo.Tipo;
import br.senai.sp.helpdesk.tables.TableModelEquipamento;

public class JanelaEquipamento extends JInternalFrame {
	private DaoEquipamento dao;
	private Color corPadrao;
	private Color corFonte;
	private Font fontePadrao;
	private JLabel lbNome;
	private JTextField tfNome;
	private JComboBox<Tipo> cbTipo;
	private Component lbTipo;
	private JButton btSalvar;
	private JButton btLimpar;
	private JButton btExcluir;
	private JLabel lbEquipamentos;
	private JTable tbEquipamentos;
	private JScrollPane spEquipamentos;
	private TableModelEquipamento model;
	private Equipamento equipamento;
	private JTextField tfBuscar;
	private JButton btBuscar;

	public JanelaEquipamento() {
		inicializarComponentes();
		definirEventos();
	}

	private void inicializarComponentes() {
		try {
			dao = new DaoEquipamento();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, "Erro: " + e1.getMessage(),
					"Erro", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();

		}

		Set<KeyStroke> teclas = new HashSet<KeyStroke>();
		teclas.add(KeyStroke.getKeyStroke("TAB"));

		corPadrao = new Color(180, 180, 180);

		corFonte = new Color(255, 255, 255);

		fontePadrao = new Font("System", Font.BOLD, 14);

		lbNome = new JLabel("Nome:");
		lbNome.setSize(80, 25);
		lbNome.setLocation(10, 10);

		tfNome = new JTextField();
		tfNome.setBounds(100, 10, 250, 25);

		lbTipo = new JLabel("Tipo:");
		lbTipo.setSize(80, 25);
		lbTipo.setLocation(10, 45);

		cbTipo = new JComboBox<Tipo>(Tipo.values());
		cbTipo.setBounds(100, 45, 250, 25);
		cbTipo.setSelectedIndex(-1);

		btSalvar = new JButton("Salvar");
		btSalvar.setFont(fontePadrao);

		btLimpar = new JButton("Limpar");
		btLimpar.setFont(fontePadrao);

		btExcluir = new JButton("Excluir");
		btExcluir.setFont(fontePadrao);

		JPanel pnBotoes = new JPanel(new GridLayout(1, 3));
		pnBotoes.add(btSalvar);
		pnBotoes.add(btExcluir);
		pnBotoes.add(btLimpar);
		pnBotoes.setBounds(0, 80, 370, 25);

		tfBuscar = new JTextField();
		tfBuscar.setBounds(0, 115, 340, 25);
		tfBuscar.setFont(fontePadrao);

		btBuscar = new JButton();
		btBuscar.setIcon(new ImageIcon(getClass().getResource(
				"/Imagens/search.png")));

		JPanel pnBuscar = new JPanel(new GridLayout(1, 1));
		pnBuscar.add(btBuscar);
		pnBuscar.setBounds(345, 115, 25, 25);
		pnBotoes.setBackground(Color.black);
		pnBuscar.setBackground(Color.black);

		tbEquipamentos = new JTable();

		spEquipamentos = new JScrollPane(tbEquipamentos);
		spEquipamentos.setBounds(0, 150, 370, 220);
		spEquipamentos
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		// parametros do frame
		// setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(380, 400);
		setTitle("EQUIPAMENTOS");
		setClosable(true);
		setMaximizable(true);
		setResizable(false);
		setLocation(0, 200);
		setMaximizable(false);
		getContentPane().setBackground(corPadrao);
		getRootPane().setDefaultButton(btSalvar);
		setContentPane(new Fundo());
		// setFrameIcon(new
		// ImageIcon(getClass().getResource("/Imagens/icon_Aluno.png")));

		try {
			criartabela(dao.listar());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Erro ao listar Equipamentos",
					"Erro", JOptionPane.ERROR_MESSAGE);
		}

		// ------------------------------------------------------------//
		setLayout(null);
		add(lbNome);
		add(tfNome);
		add(lbTipo);
		add(cbTipo);
		add(spEquipamentos);
		add(pnBotoes);
		add(pnBuscar);
		add(tfBuscar);
		setVisible(true);
		for (Component c : getContentPane().getComponents()) {
			c.setFont(fontePadrao);
			if (c instanceof JLabel) {
				c.setForeground(corFonte);
			}
		}

	}

	private class Fundo extends JDesktopPane {
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D graph = (Graphics2D) g;
			Image imagem = new ImageIcon(getClass().getResource(
					"/Imagens/D-edge.jpg")).getImage();
			graph.drawImage(imagem, 0, 0, this.getWidth(), this.getHeight(),
					this);
		}
	}

	private void definirEventos() {
		btBuscar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!tfBuscar.getText().trim().isEmpty()) {
					try {
						criartabela(dao.buscar(tfBuscar.getText().toString()));
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(JanelaEquipamento.this,
								"Erro ao buscar", "Erro",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btLimpar.addActionListener(e -> {
			limpar();
		});

		btExcluir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (equipamento != null) {
					if (JOptionPane.showConfirmDialog(JanelaEquipamento.this,
							"Deseja realmente excluir o equipamento "
									+ equipamento.getNome() + "?",
							"Confirmar exclus�o", JOptionPane.YES_NO_OPTION) == 0) {
						try {
							dao.excluir(equipamento.getId());
							criartabela(dao.listar());
							limpar();
						} catch (Exception e2) {
							JOptionPane.showMessageDialog(
									JanelaEquipamento.this, e2.getMessage());
						}
					}
				}

			}
		});

		tbEquipamentos.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent e) {
						int linhaSelecionada = tbEquipamentos.getSelectedRow();
						if (linhaSelecionada >= 0) {
							equipamento = model
									.getEquipamento(linhaSelecionada);
							tfNome.setText(equipamento.getNome());
							cbTipo.setSelectedItem(equipamento.getTipo());
						}

					}
				});

		btSalvar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (tfNome.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(JanelaEquipamento.this,
							"Nome inv�lido", "Erro", JOptionPane.ERROR_MESSAGE);
					tfNome.requestFocus();
				} else if (cbTipo.getSelectedIndex() < 0) {
					JOptionPane
							.showMessageDialog(JanelaEquipamento.this,
									"Informe o tipo", "Erro",
									JOptionPane.ERROR_MESSAGE);
				} else {
					if (equipamento == null) {
						equipamento = new Equipamento();
						equipamento.setNome(tfNome.getText());
						equipamento.setTipo((Tipo) cbTipo.getSelectedItem());
						try {
							dao.inserir(equipamento);
							limpar();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(
									JanelaEquipamento.this, "Erro ao salvar:"
											+ e1.getMessage(), "Erro",
									JOptionPane.ERROR_MESSAGE);
						}
					} else {
						equipamento.setNome(tfNome.getText());
						equipamento.setTipo((Tipo) cbTipo.getSelectedItem());
						try {
							dao.alterar(equipamento);
							limpar();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(
									JanelaEquipamento.this, "Erro ao alterar:"
											+ e1.getMessage(), "Erro",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				try {
					criartabela(dao.listar());
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(JanelaEquipamento.this,
							"Erro: " + e1.getMessage(), "Erro",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

	}

	private void criartabela(List<Equipamento> equipamentos) {
		model = new TableModelEquipamento(equipamentos);
		tbEquipamentos.setModel(model);
		tbEquipamentos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tbEquipamentos.setSelectionBackground(Color.BLACK);
		tbEquipamentos.getColumnModel().getColumn(0).setPreferredWidth(70);
		tbEquipamentos.getColumnModel().getColumn(1).setPreferredWidth(150);
		tbEquipamentos.getColumnModel().getColumn(2).setPreferredWidth(128);
		tbEquipamentos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tbEquipamentos.setRowHeight(25);
		tbEquipamentos.getTableHeader().setReorderingAllowed(false);
		tbEquipamentos.getTableHeader().setResizingAllowed(false);

		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		tbEquipamentos.getColumnModel().getColumn(0).setCellRenderer(render);
	}

	private void limpar() {
		equipamento = null;
		cbTipo.setSelectedIndex(-1);
		tfNome.setText(null);
	}

}

package br.senai.sp.helpdesk.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.text.MaskFormatter;

import br.senai.sp.helpdesk.dao.DaoChamado;
import br.senai.sp.helpdesk.dao.DaoEquipamento;
import br.senai.sp.helpdesk.dao.DaoProblema;
import br.senai.sp.helpdesk.document.DescricaoDocument;
import br.senai.sp.helpdesk.modelo.Chamado;
import br.senai.sp.helpdesk.modelo.Equipamento;
import br.senai.sp.helpdesk.modelo.Problema;
import br.senai.sp.helpdesk.modelo.Solicitante;
import br.senai.sp.helpdesk.tables.TableModelChamado;

public class JanelaChamado extends JInternalFrame {
	DaoChamado dao;
	private Color corPadrao;
	private Color corFonte;
	private Font fontePadrao;
	private Component lbEquipamento;
	private JComboBox<Equipamento> cbEquipamento;
	private DaoEquipamento daoEquipamento;
	private JLabel lbProblema;
	private JComboBox<Problema> cbProblema;
	private JLabel lbLocalizacao;
	private JTextField tfLocalizacao;
	private JLabel lbDescricao;
	private JTextArea taDescricao;
	private JScrollPane spDescricao;
	private JLabel lbData;
	private MaskFormatter maskData;
	private JFormattedTextField tfData;
	private JLabel lbDescricao1;
	private JButton btLimpar;
	private JButton btEnviar;
	private JTable tbChamados;
	private JScrollPane spChamados;
	private JLabel lbChamados;
	private TableModelChamado model;
	private Chamado chamado;
	private DaoProblema daoProblema;
	private Solicitante s;

	public JanelaChamado(Solicitante s) {
		this.s = s;
		inicializarComponentes();
		definirEventos();

	}

	private void inicializarComponentes() {
		try {
			dao = new DaoChamado();
			daoEquipamento = new DaoEquipamento();
			daoProblema = new DaoProblema();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, "Erro: " + e1.getMessage(),
					"Erro", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();

		}

		corPadrao = new Color(180, 180, 180);

		corFonte = new Color(255, 255, 255);

		fontePadrao = new Font("System", Font.BOLD, 14);

		lbEquipamento = new JLabel("Equipamento:");
		lbEquipamento.setSize(200, 25);
		lbEquipamento.setLocation(10, 10);
		try {
			cbEquipamento = new JComboBox<Equipamento>(new Vector<>(
					daoEquipamento.listar()));
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		cbEquipamento.setBounds(120, 10, 250, 25);
		cbEquipamento.setSelectedIndex(-1);
		//cbEquipamento.setBackground(Color.gray);

		lbProblema = new JLabel("Problema:");
		lbProblema.setSize(100, 25);
		lbProblema.setLocation(10, 45);

		try {
			cbProblema = new JComboBox<Problema>(new Vector<>(
					daoProblema.listar()));
		} catch (Exception e) {
			/*
			 * JOptionPane.showMessageDialog(JanelaChamado.this, "Erro", "Erro",
			 * JOptionPane.ERROR_MESSAGE);
			 */
			e.printStackTrace();
		}
		cbProblema.setBounds(120, 45, 250, 25);
		cbProblema.setSelectedIndex(-1);

		lbLocalizacao = new JLabel("Localiza��o:");
		lbLocalizacao.setSize(100, 25);
		lbLocalizacao.setLocation(10, 80);

		tfLocalizacao = new JTextField();
		tfLocalizacao.setBounds(120, 80, 250, 25);
		tfLocalizacao.setFont(fontePadrao);

		lbDescricao = new JLabel("Descri��o");
		lbDescricao.setSize(100, 25);
		lbDescricao.setLocation(10, 115);
		lbDescricao1 = new JLabel("Adicional:");
		lbDescricao1.setSize(100, 25);
		lbDescricao1.setLocation(10, 135);

		Set<KeyStroke> teclas = new HashSet<KeyStroke>();
		teclas.add(KeyStroke.getKeyStroke("TAB"));

		taDescricao = new JTextArea();
		taDescricao.setFont(fontePadrao);
		taDescricao.setLineWrap(true);
		taDescricao.setWrapStyleWord(true);
		taDescricao.setFont(fontePadrao);
		taDescricao.setFocusTraversalKeys(
				KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, teclas);
		taDescricao.setDocument(new DescricaoDocument());

		spDescricao = new JScrollPane(taDescricao);
		spDescricao.setBounds(120, 115, 250, 100);
		spDescricao
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		lbData = new JLabel("Data:");
		lbData.setLocation(10, 225);
		lbData.setSize(100, 25);

		try {
			maskData = new MaskFormatter("##/##/####");
			maskData.setPlaceholderCharacter('_');
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Erro",
					JOptionPane.ERROR_MESSAGE);
		}

		tfData = new JFormattedTextField(maskData);
		tfData.setBounds(120, 225, 250, 25);
		tfData.setHorizontalAlignment(SwingConstants.CENTER);

		btEnviar = new JButton("Enviar");
		btEnviar.setFont(fontePadrao);

		btLimpar = new JButton("Limpar");
		btLimpar.setFont(fontePadrao);

		JPanel pnBotoes = new JPanel(new GridLayout(1, 2));
		pnBotoes.add(btEnviar);
		pnBotoes.add(btLimpar);
		pnBotoes.setBounds(0, 260, 370, 25);
		pnBotoes.setBackground(Color.black);

		lbChamados = new JLabel("Lista de chamados:");
		lbChamados.setLocation(410, 10);
		lbChamados.setSize(1000, 25);

		tbChamados = new JTable();

		spChamados = new JScrollPane(tbChamados);
		spChamados.setBounds(400, 45, 400, 240);
		spChamados
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		// parametros do frame
		// setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(810, 315);
		setTitle("NOVO CHAMADO - " + s.getNome());
		setClosable(true);
		setMaximizable(false);
		setResizable(false);
		getContentPane().setBackground(corPadrao);
		getRootPane().setDefaultButton(btEnviar);
		setContentPane(new Fundo());
		// setFrameIcon(new
		// ImageIcon(getClass().getResource("/Imagens/icon_Aluno.png")));

		try {
			criartabela(dao.listarUser(s));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Erro ao listar Chamados",
					"Erro", JOptionPane.ERROR_MESSAGE);
		}

		// ------------------------------------------------------------//
		setLayout(null);
		add(lbEquipamento);
		add(cbEquipamento);
		add(lbProblema);
		add(cbProblema);
		add(lbLocalizacao);
		add(tfLocalizacao);
		add(lbDescricao);
		add(lbDescricao1);
		add(spDescricao);
		add(lbData);
		add(tfData);
		add(pnBotoes);
		add(spChamados);
		add(lbChamados);
		setVisible(true);
		
		for (Component c : getContentPane().getComponents()) {
			c.setFont(fontePadrao);
			if (c instanceof JLabel) {
				c.setForeground(corFonte);
			}
		}
	}

	private class Fundo extends JDesktopPane {
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D graph = (Graphics2D) g;
			Image imagem = new ImageIcon(getClass().getResource(
					"/Imagens/D-edge.jpg")).getImage();
			graph.drawImage(imagem, 0, 0, this.getWidth(), this.getHeight(),
					this);
		}
	}

	private void definirEventos() {
		btLimpar.addActionListener(e -> {
			limpar();
		});

		btEnviar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (cbEquipamento.getSelectedIndex() < 0) {
					JOptionPane.showMessageDialog(JanelaChamado.this,
							"Selecione o Equipamento", "Erro",
							JOptionPane.ERROR_MESSAGE);
				} else if (cbProblema.getSelectedIndex() < 0) {
					JOptionPane.showMessageDialog(JanelaChamado.this,
							"Selecione o Problema", "Erro",
							JOptionPane.ERROR_MESSAGE);
					// tfDescricao.setBackground(Color.RED);
				} else if (tfLocalizacao.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(JanelaChamado.this,
							"Localiza��o inv�lida", "Erro",
							JOptionPane.ERROR_MESSAGE);
					tfLocalizacao.requestFocus();
				} else if (taDescricao.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(JanelaChamado.this,
							"Informe a descri��o", "Erro",
							JOptionPane.ERROR_MESSAGE);
				} else if (tfData.getValue() == null) {
					JOptionPane.showMessageDialog(JanelaChamado.this,
							"Data inv�lida", "Erro", JOptionPane.ERROR_MESSAGE);
					tfData.requestFocus();
				} else {
					if (chamado == null) {
						chamado = new Chamado();
						chamado.setEquipamento((Equipamento) cbEquipamento
								.getSelectedItem());
						chamado.setProblema((Problema) cbProblema
								.getSelectedItem());
						chamado.setLocalizacao(tfLocalizacao.getText().trim());
						chamado.setDescricao(taDescricao.getText().trim());
						Calendar calendar = Calendar.getInstance();
						SimpleDateFormat formatador = new SimpleDateFormat(
								"dd/MM/yyyy");
						try {
							Date dateNasc = formatador.parse(tfData.getValue()
									.toString());
							calendar.setTime(dateNasc);
							chamado.setData(calendar);
						} catch (ParseException e1) {
							JOptionPane.showMessageDialog(JanelaChamado.this,
									"Erro de convers�o:" + e1.getMessage(),
									"Erro", JOptionPane.ERROR_MESSAGE);
						}
						chamado.setTecnico(null);
						chamado.setStatus(true);
						chamado.setSolicitante(s);
						try {
							dao.inserir(chamado);
							limpar();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(JanelaChamado.this,
									"Erro ao salvar:" + e1.getMessage(),
									"Erro", JOptionPane.ERROR_MESSAGE);

						}
					} else {
						JOptionPane.showMessageDialog(JanelaChamado.this,
								"Chamado j� existente", "Erro",
								JOptionPane.ERROR_MESSAGE);
					}
				}

				try {
					criartabela(dao.listarUser(s));
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(JanelaChamado.this, "Erro: "
							+ e1.getMessage(), "Erro",
							JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
			}
		});

	}

	private void criartabela(List<Chamado> chamados) {
		model = new TableModelChamado(chamados);
		tbChamados.setModel(model);
		tbChamados.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tbChamados.setSelectionBackground(Color.BLACK);
		tbChamados.getColumnModel().getColumn(0).setPreferredWidth(30);
		tbChamados.getColumnModel().getColumn(1).setPreferredWidth(90);
		tbChamados.getColumnModel().getColumn(2).setPreferredWidth(70);
		tbChamados.getColumnModel().getColumn(3).setPreferredWidth(90);
		tbChamados.getColumnModel().getColumn(4).setPreferredWidth(50);
		tbChamados.getColumnModel().getColumn(5).setPreferredWidth(90);
		tbChamados.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tbChamados.setRowHeight(25);
		tbChamados.getTableHeader().setReorderingAllowed(false);
		tbChamados.getTableHeader().setResizingAllowed(false);

		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		tbChamados.getColumnModel().getColumn(0).setCellRenderer(render);
	}

	private void limpar() {
		chamado = null;
		cbEquipamento.setSelectedIndex(-1);
		cbProblema.setSelectedIndex(-1);
		tfLocalizacao.setText(null);
		taDescricao.setText(null);
		tfData.setValue(null);
	}
}

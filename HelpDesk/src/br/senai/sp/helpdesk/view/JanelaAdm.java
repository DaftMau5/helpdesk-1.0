package br.senai.sp.helpdesk.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class JanelaAdm extends JInternalFrame {

	private Color corPadrao;
	private Color corFonte;
	private Font fontePadrao;
	private JButton btSolicitantes;
	private JButton btTecnicos;
	private JButton btEquipamentos;
	private JButton btChamados;
	private JButton btProblemas;
	private JButton btAdms;

	public JanelaAdm() {
		inicializarComponentes();
		definirEventos();
	}

	private void inicializarComponentes() {
		corPadrao = new Color(180, 180, 180);

		corFonte = new Color(255, 255, 255);

		fontePadrao = new Font("System", Font.BOLD, 14);

		btChamados = new JButton("Chamados");
		btChamados.setFont(fontePadrao);
		btChamados.setBackground(Color.black);
		btChamados.setForeground(corFonte);
		
		btAdms = new JButton("Adms");
		btAdms.setFont(fontePadrao);
		btAdms.setBackground(Color.black);
		btAdms.setForeground(corFonte);

		btSolicitantes = new JButton("Solicitantes");
		btSolicitantes.setFont(fontePadrao);
		btSolicitantes.setBackground(Color.black);
		btSolicitantes.setForeground(corFonte);

		btProblemas = new JButton("Problemas");
		btProblemas.setFont(fontePadrao);
		btProblemas.setBackground(Color.black);
		btProblemas.setForeground(corFonte);

		btTecnicos = new JButton("T�cnicos");
		btTecnicos.setFont(fontePadrao);
		btTecnicos.setBackground(Color.black);
		btTecnicos.setForeground(corFonte);

		btEquipamentos = new JButton("Equipamentos");
		btEquipamentos.setFont(fontePadrao);
		btEquipamentos.setBackground(Color.black);
		btEquipamentos.setForeground(corFonte);

		JPanel pnBotoes = new JPanel(new GridLayout(1, 5));
		pnBotoes.add(btSolicitantes);
		pnBotoes.add(btProblemas);
		pnBotoes.add(btTecnicos);
		pnBotoes.add(btEquipamentos);
		pnBotoes.add(btAdms);
		pnBotoes.add(btChamados);
		pnBotoes.setBounds(0, 0, 1200, 100);
		pnBotoes.setBackground(Color.black);
		
		btChamados.setIcon(new ImageIcon(getClass().getResource("/Imagens/admChamados.png")));
		btSolicitantes.setIcon(new ImageIcon(getClass().getResource("/Imagens/admSolicitantes.png")));
		btProblemas.setIcon(new ImageIcon(getClass().getResource("/Imagens/admProblemas.png")));
		btTecnicos.setIcon(new ImageIcon(getClass().getResource("/Imagens/admTec.png")));
		btEquipamentos.setIcon(new ImageIcon(getClass().getResource("/Imagens/admEquipamentos.png")));
		btAdms.setIcon(new ImageIcon(getClass().getResource("/Imagens/AdmAdm.png")));

		// parametros do frame
		// setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(1212, 130);
		setTitle("AREA DO ADM");
		setClosable(true);
		setMaximizable(false);
		setResizable(false);
		getContentPane().setBackground(corPadrao);
		setContentPane(new Fundo());
		// setFrameIcon(new
		// ImageIcon(getClass().getResource("/Imagens/icon_Aluno.png")));

		setLayout(null);
		add(pnBotoes);
		setVisible(true);

		for (Component c : getContentPane().getComponents()) {
			c.setFont(fontePadrao);
			if (c instanceof JLabel) {
				c.setForeground(corFonte);
			}
		}
	}

	private class Fundo extends JDesktopPane {
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D graph = (Graphics2D) g;
			Image imagem = new ImageIcon(getClass().getResource(
					"/Imagens/D-edge.jpg")).getImage();
			graph.drawImage(imagem, 0, 0, this.getWidth(), this.getHeight(),
					this);
		}
	}

	private void definirEventos() {
		btEquipamentos.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				getDesktopPane().add(new JanelaEquipamento());

			}
		});
		btChamados.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				getDesktopPane().add(new JanelaChamadoAdm());

			}
		});
		btTecnicos.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				getDesktopPane().add(new JanelaTecAdm());

			}
		});
		btProblemas.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				getDesktopPane().add(new JanelaProblema());

			}
		});
		btSolicitantes.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				getDesktopPane().add(new JanelaSolicitante());

			}
		});
		btAdms.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				getDesktopPane().add(new JanelaAdmAdm());

			}
		});

	}

}

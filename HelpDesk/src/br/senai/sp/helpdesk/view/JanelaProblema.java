package br.senai.sp.helpdesk.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;

import br.senai.sp.helpdesk.dao.DaoProblema;
import br.senai.sp.helpdesk.modelo.Problema;
import br.senai.sp.helpdesk.modelo.Tipo;
import br.senai.sp.helpdesk.tables.TableModelProblema;

public class JanelaProblema extends JInternalFrame {
	private DaoProblema dao;
	private Color corPadrao;
	private Color corFonte;
	private Font fontePadrao;
	private JLabel lbNome;
	private JTextField tfNome;
	private JComboBox<Tipo> cbTipo;
	private Component lbTipo;
	private JButton btSalvar;
	private JButton btLimpar;
	private JButton btExcluir;
	private JLabel lbEquipamentos;
	private JTable tbProblemas;
	private JScrollPane spEquipamentos;
	private TableModelProblema model;
	private Problema problema;
	private JTextField tfBuscar;
	private JButton btBuscar;

	public JanelaProblema() {
		inicializarComponentes();
		definirEventos();
	}

	private void inicializarComponentes() {
		try {
			dao = new DaoProblema();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, "Erro: " + e1.getMessage(),
					"Erro", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();

		}

		Set<KeyStroke> teclas = new HashSet<KeyStroke>();
		teclas.add(KeyStroke.getKeyStroke("TAB"));

		corPadrao = new Color(180, 180, 180);

		corFonte = new Color(255, 255, 255);

		fontePadrao = new Font("System", Font.BOLD, 14);

		lbNome = new JLabel("Nome:");
		lbNome.setSize(80, 25);
		lbNome.setLocation(10, 10);

		tfNome = new JTextField();
		tfNome.setBounds(100, 10, 250, 25);

		btSalvar = new JButton("Salvar");
		btSalvar.setFont(fontePadrao);

		btLimpar = new JButton("Limpar");
		btLimpar.setFont(fontePadrao);

		btExcluir = new JButton("Excluir");
		btExcluir.setFont(fontePadrao);

		JPanel pnBotoes = new JPanel(new GridLayout(1, 3));
		pnBotoes.add(btSalvar);
		pnBotoes.add(btExcluir);
		pnBotoes.add(btLimpar);
		pnBotoes.setBounds(0, 45, 370, 25);
		

		tfBuscar = new JTextField();
		tfBuscar.setBounds(0, 80, 340, 25);
		tfBuscar.setFont(fontePadrao);

		btBuscar = new JButton();
		btBuscar.setIcon(new ImageIcon(getClass().getResource(
				"/Imagens/search.png")));

		JPanel pnBuscar = new JPanel(new GridLayout(1, 1));
		pnBuscar.add(btBuscar);
		pnBuscar.setBounds(345, 80, 25, 25);
		pnBotoes.setBackground(Color.black);
		pnBuscar.setBackground(Color.black);

		tbProblemas = new JTable();

		spEquipamentos = new JScrollPane(tbProblemas);
		spEquipamentos.setBounds(0, 115, 370, 220);
		spEquipamentos
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		// parametros do frame
		// setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(380, 365);
		setTitle("PROBLEMAS");
		setClosable(true);
		setMaximizable(true);
		setResizable(false);
		setLocation(0, 200);
		setMaximizable(false);
		getContentPane().setBackground(corPadrao);
		getRootPane().setDefaultButton(btSalvar);
		setContentPane(new Fundo());
		// setFrameIcon(new
		// ImageIcon(getClass().getResource("/Imagens/icon_Aluno.png")));

		try {
			criartabela(dao.listar());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Erro ao listar Problemas",
					"Erro", JOptionPane.ERROR_MESSAGE);
		}

		// ------------------------------------------------------------//
		setLayout(null);
		add(lbNome);
		add(tfNome);
		add(spEquipamentos);
		add(pnBuscar);
		add(tfBuscar);
		add(pnBotoes);
		setVisible(true);

		for (Component c : getContentPane().getComponents()) {
			c.setFont(fontePadrao);
			if (c instanceof JLabel) {
				c.setForeground(corFonte);
			}
		}

	}

	private class Fundo extends JDesktopPane {
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D graph = (Graphics2D) g;
			Image imagem = new ImageIcon(getClass().getResource(
					"/Imagens/D-edge.jpg")).getImage();
			graph.drawImage(imagem, 0, 0, this.getWidth(), this.getHeight(),
					this);
		}
	}

	private void definirEventos() {
		btBuscar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!tfBuscar.getText().trim().isEmpty()) {
					try {
						criartabela(dao.buscar(tfBuscar.getText().toString()));
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(JanelaProblema.this,
								"Erro ao buscar", "Erro",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btLimpar.addActionListener(e -> {
			limpar();
		});

		btExcluir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (problema != null) {
					if (JOptionPane.showConfirmDialog(
							JanelaProblema.this,
							"Deseja realmente excluir o problema "
									+ problema.getNome() + "?",
							"Confirmar exclus�o", JOptionPane.YES_NO_OPTION) == 0) {
						try {
							dao.excluir(problema.getId());
							criartabela(dao.listar());
							limpar();
						} catch (Exception e2) {
							JOptionPane.showMessageDialog(JanelaProblema.this,
									e2.getMessage());
						}
					}
				}

			}
		});

		tbProblemas.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent e) {
						int linhaSelecionada = tbProblemas.getSelectedRow();
						if (linhaSelecionada >= 0) {
							problema = model.getProblema(linhaSelecionada);
							tfNome.setText(problema.getNome());
						}

					}
				});

		btSalvar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (tfNome.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(JanelaProblema.this,
							"Nome inv�lido", "Erro", JOptionPane.ERROR_MESSAGE);
					tfNome.requestFocus();
				} else {
					if (problema == null) {
						problema = new Problema();
						problema.setNome(tfNome.getText());
						try {
							dao.inserir(problema);
							limpar();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(JanelaProblema.this,
									"Erro ao salvar:" + e1.getMessage(),
									"Erro", JOptionPane.ERROR_MESSAGE);
						}
					} else {
						problema.setNome(tfNome.getText());
						try {
							dao.alterar(problema);
							limpar();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(JanelaProblema.this,
									"Erro ao alterar:" + e1.getMessage(),
									"Erro", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				try {
					criartabela(dao.listar());
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(JanelaProblema.this, "Erro: "
							+ e1.getMessage(), "Erro",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

	}

	private void criartabela(List<Problema> problema) {
		model = new TableModelProblema(problema);
		tbProblemas.setModel(model);
		tbProblemas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tbProblemas.setSelectionBackground(Color.BLACK);
		tbProblemas.getColumnModel().getColumn(0).setPreferredWidth(100);
		tbProblemas.getColumnModel().getColumn(1).setPreferredWidth(248);
		tbProblemas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tbProblemas.setRowHeight(25);
		tbProblemas.getTableHeader().setReorderingAllowed(false);
		tbProblemas.getTableHeader().setResizingAllowed(false);

		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		tbProblemas.getColumnModel().getColumn(0).setCellRenderer(render);
	}

	private void limpar() {
		problema = null;
		tfNome.setText(null);
	}

}

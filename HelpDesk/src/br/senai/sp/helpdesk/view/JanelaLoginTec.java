package br.senai.sp.helpdesk.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import br.senai.sp.helpdesk.dao.DaoTec;
import br.senai.sp.helpdesk.modelo.Tecnico;

public class JanelaLoginTec extends JInternalFrame {
	private JTextField tfLogin;
	private JLabel lbSenha;
	private JLabel lbLogin;
	private JButton btLogar;
	private JButton btCancelar;
	private JPasswordField pfSenha;
	private static JanelaLoginTec frame;
	public Color corPadrao, corFonte;
	Font fontePadrao;
	private DaoTec dao;
	private Tecnico s;

	public JanelaLoginTec() {
		inicializarComponentes();
		definirEventos();
	}

	private void inicializarComponentes() {
		try {
			dao = new DaoTec();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, "Erro: " + e1.getMessage(),
					"Erro", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();

		}
		setTitle("Login do T�cnico");
		setBounds(0, 0, 520, 110);
		setLayout(null);
		setVisible(true);
		corPadrao = new Color(180, 180, 180);
		corFonte = new Color(255, 255, 255);
		fontePadrao = new Font("System", Font.BOLD, 14);

		tfLogin = new JTextField(5);
		pfSenha = new JPasswordField(5);
		lbSenha = new JLabel("Senha:");
		lbLogin = new JLabel("Login:");
		btLogar = new JButton("Logar");
		btCancelar = new JButton("Cancelar");

		tfLogin.setBounds(60, 10, 180, 25);
		lbLogin.setBounds(10, 10, 50, 25);
		lbSenha.setBounds(260, 10, 50, 25);
		pfSenha.setBounds(320, 10, 180, 25);
		btLogar.setBounds(0, 45, 253, 25);
		btLogar.setBackground(Color.black);
		btLogar.setForeground(Color.cyan);
		btCancelar.setBounds(256, 45, 253, 25);
		btCancelar.setBackground(Color.black);
		btCancelar.setForeground(corFonte);
		getRootPane().setDefaultButton(btLogar);
		getContentPane().setBackground(corPadrao);
		setContentPane(new Fundo());

		add(tfLogin);
		add(lbSenha);
		add(lbLogin);
		add(btLogar);
		add(btCancelar);
		add(pfSenha);

		for (Component c : getContentPane().getComponents()) {
			c.setFont(fontePadrao);
			if (c instanceof JLabel) {
				c.setForeground(corFonte);
			}
		}

	}

	private class Fundo extends JDesktopPane {
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D graph = (Graphics2D) g;
			Image imagem = new ImageIcon(getClass().getResource(
					"/Imagens/D-edge.jpg")).getImage();
			graph.drawImage(imagem, 0, 0, this.getWidth(), this.getHeight(),
					this);
		}
	}

	private void definirEventos() {
		btLogar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tecnico s;
				try {
					s = dao.login(tfLogin.getText(),
							new String(pfSenha.getPassword()));
					if (s != null) {
						getDesktopPane().add(new JanelaTecArea(s));
						dispose();
					} else {
						JOptionPane.showMessageDialog(null,
								"Login ou senha incorretas!");
					}
				} catch (FileNotFoundException | ParseException e1) {
					e1.printStackTrace();
				}

			}
		});

		btCancelar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				frame = new JanelaLoginTec();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				Dimension tela = Toolkit.getDefaultToolkit().getScreenSize();
				frame.setLocation((tela.width - frame.getSize().width) / 2,
						(tela.height - frame.getSize().height) / 2);
				frame.setVisible(true);
			}
		});
	}
}

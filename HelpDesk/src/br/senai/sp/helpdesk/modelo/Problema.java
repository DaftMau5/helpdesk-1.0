package br.senai.sp.helpdesk.modelo;

public class Problema {
	private int id;
	private String nome;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String toString() {
		// TODO Auto-generated method stub
		return nome;
	}
	public String exibir(){
		return (id + ";" + nome);
	}
	
	@Override
	public boolean equals(Object problema) {
		if (problema == null || problema.getClass() != this.getClass()) {
			return false;
		}
		Problema prob = (Problema) problema;
		if (this.getId() == prob.getId()) {
			return true;
		}else{
			return false;
		}
		
	}
}

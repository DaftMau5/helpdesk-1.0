package br.senai.sp.helpdesk.modelo;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Chamado {
	private int id;
	private Equipamento equipamento;
	private Problema problema;
	private String localizacao;
	private String descricao;
	private Calendar data;
	private Tecnico tecnico;
	private Solicitante solicitante;

	public Solicitante getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(Solicitante solicitante) {
		this.solicitante = solicitante;
	}

	private boolean status;

	public Tecnico getTecnico() {
		return tecnico;
	}

	public void setTecnico(Tecnico tecnico) {
		this.tecnico = tecnico;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Equipamento getEquipamento() {
		return equipamento;
	}

	public void setEquipamento(Equipamento equipamento) {
		this.equipamento = equipamento;
	}

	public Problema getProblema() {
		return problema;
	}

	public void setProblema(Problema problema) {
		this.problema = problema;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String exibir() {
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		return (id + ";" + equipamento.getId() + ";" + problema.getId() + ";"
				+ localizacao + ";" + descricao + ";"
				+ formatador.format(data.getTime()) + ";"
				+ (tecnico != null ? tecnico.getId() + "" : 0) + ";" + status
				+ ";" + solicitante.getId());
	}

}

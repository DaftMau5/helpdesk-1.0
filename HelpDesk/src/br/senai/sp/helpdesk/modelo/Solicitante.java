package br.senai.sp.helpdesk.modelo;

public class Solicitante {
	private int id;
	private String nome;
	private String email;
	private String usuario;
	private String senha;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String exibir(){
		return (id + ";" + nome+ ";" +email+ ";" +usuario+ ";" +senha);
	}
	public boolean equals(Object solicitante) {
		if (solicitante == null || solicitante.getClass() != this.getClass()) {
			return false;
		}
		Solicitante sol = (Solicitante) solicitante;
		if (this.getId() == sol.getId()) {
			return true;
		}else{
			return false;
		}
		
	}
	public String toString() {
		return nome;
	}
}

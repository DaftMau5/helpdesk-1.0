package br.senai.sp.helpdesk.modelo;

public class Tecnico {
	private int id;
	private String nome;
	private Tipo tipo;
	private String email;
	private String usuario;
	private String senha;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String exibir() {
		return (id + ";" + nome + ";" + tipo.ordinal() + ";" + email + ";"
				+ usuario + ";" + senha);
	}

	public String toString() {
		return nome+" - "+tipo;
	}

	public boolean equals(Object tecnico) {
		if (tecnico == null || tecnico.getClass() != this.getClass()) {
			return false;
		}
		Tecnico tec = (Tecnico) tecnico;
		if (this.getId() == tec.getId()) {
			return true;
		} else {
			return false;
		}

	}

}

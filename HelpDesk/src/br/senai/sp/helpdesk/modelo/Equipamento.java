package br.senai.sp.helpdesk.modelo;

public class Equipamento {
	private int id;
	private String nome;
	private Tipo tipo;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Tipo getTipo() {
		return tipo;
	}
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
	public String toString() {
		return nome;
	}
	public String exibir(){
		return (id + ";" + nome+ ";" +tipo.ordinal());
	}
	
	@Override
	public boolean equals(Object equipamento) {
		if (equipamento == null || equipamento.getClass() != this.getClass()) {
			return false;
		}
		Equipamento equip = (Equipamento) equipamento;
		if (this.getId() == equip.getId()) {
			return true;
		}else{
			return false;
		}
		
	}
}

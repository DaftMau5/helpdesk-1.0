package br.senai.sp.helpdesk.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.senai.sp.helpdesk.modelo.Problema;

public class DaoProblema {
	private static int LastId;

	static {
		File arquivo = new File("problema.csv");
		if (!arquivo.exists()) {
			try {
				arquivo.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public DaoProblema() throws FileNotFoundException {
		Scanner leitor = new Scanner(new FileReader("id_problema.txt"));
		LastId = leitor.nextInt();
		leitor.close();
	}

	public void inserir(Problema problema) throws IOException {
		problema.setId(++LastId);
		FileWriter writer = new FileWriter("problema.csv", true);
		writer.write(problema.exibir() + "\n");
		writer.close();

		writer = new FileWriter("id_problema.txt");
		writer.write(LastId + "\n");
		writer.close();
	}
	public void alterar(Problema problemaAlterar) throws IOException {
		Scanner leitor = new Scanner(new FileReader("problema.csv"));
		FileWriter writer = new FileWriter("problema.new", true);
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idLinha = Integer.parseInt(dados[0]);
			if (problemaAlterar.getId() != idLinha) {
				writer.write(linha + "\n");
			} else {
				writer.write(problemaAlterar.exibir() + "\n");
			}

		}
		writer.close();
		leitor.close();

		File fInicial = new File("problema.csv");
		fInicial.delete();

		File f = new File("problema.new");
		f.renameTo(fInicial);
	}

	public void excluir(int id) throws IOException {
		Scanner leitor = new Scanner(new FileReader("problema.csv"));
		FileWriter writer = new FileWriter("problema.new", true);
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idLinha = Integer.parseInt(dados[0]);
			if (id != idLinha) {
				writer.write(linha + "\n");
			}
		}
		writer.close();
		leitor.close();

		File fInicial = new File("problema.csv");
		fInicial.delete();

		File f = new File("problema.new");
		f.renameTo(fInicial);
	}

	public List<Problema> listar() throws FileNotFoundException,
			ParseException {
		List<Problema> lista = new ArrayList<Problema>();
		Scanner leitor = new Scanner(new FileReader("problema.csv"));
		while (leitor.hasNextLine()) {
			String linha = leitor.nextLine();
			String dados[] = linha.split(";");
			Problema a = new Problema();
			a.setId(Integer.parseInt(dados[0]));
			a.setNome(dados[1]);
			lista.add(a);
		}
		leitor.close();
		return lista;
	}

	public List<Problema> buscar(String nome) throws FileNotFoundException,
			ParseException {
		List<Problema> lista = new ArrayList<Problema>();
		Scanner leitor = new Scanner(new FileReader("problema.csv"));
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			String nomeProblema = dados[1];
			if (nomeProblema.toLowerCase().startsWith(nome.toLowerCase())) {
				Problema a = new Problema();
				a.setId(Integer.parseInt(dados[0]));
				a.setNome(dados[1]);
				lista.add(a);
			}
		}
		leitor.close();
		return lista;
	}

	public Problema buscar(int id) throws FileNotFoundException,
			ParseException {
		Scanner leitor = new Scanner(new FileReader("problema.csv"));
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idProblema = Integer.parseInt(dados[0]);
			if (id == idProblema) {
				Problema a = new Problema();
				a.setId(Integer.parseInt(dados[0]));
				a.setNome(dados[1]);
				leitor.close();
				return a;
			}
		}
		leitor.close();
		return null;
	}

}


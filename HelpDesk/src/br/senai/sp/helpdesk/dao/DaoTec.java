package br.senai.sp.helpdesk.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.senai.sp.helpdesk.modelo.Tecnico;
import br.senai.sp.helpdesk.modelo.Tipo;

public class DaoTec {
	private static int LastId;

	static {
		File arquivo = new File("tec.csv");
		if (!arquivo.exists()) {
			try {
				arquivo.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public DaoTec() throws FileNotFoundException {
		Scanner leitor = new Scanner(new FileReader("id_tec.txt"));
		LastId = leitor.nextInt();
		leitor.close();
	}

	public void inserir(Tecnico tec) throws IOException {
		tec.setId(++LastId);
		FileWriter writer = new FileWriter("tec.csv", true);
		writer.write(tec.exibir() + "\n");
		writer.close();

		writer = new FileWriter("id_tec.txt");
		writer.write(LastId + "\n");
		writer.close();
	}

	public void alterar(Tecnico tecAlterar) throws IOException {
		Scanner leitor = new Scanner(new FileReader("tec.csv"));
		FileWriter writer = new FileWriter("tec.new", true);
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idLinha = Integer.parseInt(dados[0]);
			if (tecAlterar.getId() != idLinha) {
				writer.write(linha + "\n");
			} else {
				writer.write(tecAlterar.exibir() + "\n");
			}
		}
		writer.close();
		leitor.close();

		File fInicial = new File("tec.csv");
		fInicial.delete();

		File f = new File("tec.new");
		f.renameTo(fInicial);
	}

	public void excluir(int id) throws IOException {
		Scanner leitor = new Scanner(new FileReader("tec.csv"));
		FileWriter writer = new FileWriter("tec.new", true);
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idLinha = Integer.parseInt(dados[0]);
			if (id != idLinha) {
				writer.write(linha + "\n");
			}
		}
		writer.close();
		leitor.close();

		File fInicial = new File("tec.csv");
		fInicial.delete();

		File f = new File("tec.new");
		f.renameTo(fInicial);
	}

	public List<Tecnico> listar() throws FileNotFoundException, ParseException {
		List<Tecnico> lista = new ArrayList<Tecnico>();
		Scanner leitor = new Scanner(new FileReader("tec.csv"));
		while (leitor.hasNextLine()) {
			String linha = leitor.nextLine();
			String dados[] = linha.split(";");
			Tecnico a = new Tecnico();
			a.setId(Integer.parseInt(dados[0]));
			a.setNome(dados[1]);
			a.setEmail(dados[3]);
			a.setTipo(Tipo.values()[Integer.parseInt(dados[2])]);
			a.setUsuario(dados[4]);
			a.setSenha(dados[5]);
			lista.add(a);
		}
		leitor.close();
		return lista;
	}

	public List<Tecnico> buscar(String nome) throws FileNotFoundException,
			ParseException {
		List<Tecnico> lista = new ArrayList<Tecnico>();
		Scanner leitor = new Scanner(new FileReader("tec.csv"));
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			String nomeTec = dados[1];
			if (nomeTec.toLowerCase().startsWith(nome.toLowerCase())) {
				Tecnico a = new Tecnico();
				a.setId(Integer.parseInt(dados[0]));
				a.setNome(dados[1]);
				a.setEmail(dados[3]);
				a.setTipo(Tipo.values()[Integer.parseInt(dados[2])]);
				a.setUsuario(dados[4]);
				a.setSenha(dados[5]);
				lista.add(a);
			}
		}
		leitor.close();
		return lista;
	}

	public Tecnico buscar(int id) throws FileNotFoundException, ParseException {
		Scanner leitor = new Scanner(new FileReader("tec.csv"));
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idTec = Integer.parseInt(dados[0]);
			if (id == idTec) {
				Tecnico a = new Tecnico();
				a.setId(Integer.parseInt(dados[0]));
				a.setNome(dados[1]);
				a.setEmail(dados[3]);
				a.setTipo(Tipo.values()[Integer.parseInt(dados[2])]);
				a.setUsuario(dados[4]);
				a.setSenha(dados[5]);
				leitor.close();
				return a;
			}
		}
		leitor.close();
		return null;
	}
	public Tecnico login(String usuario, String senha)
			throws FileNotFoundException, ParseException {
		Scanner leitor = new Scanner(new FileReader("tec.csv"));
		Tecnico a = null;
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			String userSol = dados[4];
			String senhaSol = dados[5];
			if (userSol.equals(usuario) && senhaSol.equals(senha)) {
				a = new Tecnico();
				a.setId(Integer.parseInt(dados[0]));
				a.setNome(dados[1]);
				a.setEmail(dados[3]);
				a.setTipo(Tipo.values()[Integer.parseInt(dados[2])]);
				a.setUsuario(dados[4]);
				a.setSenha(dados[5]);
				leitor.close();
				break;
			}
		}
		leitor.close();
		return a;
	}
}

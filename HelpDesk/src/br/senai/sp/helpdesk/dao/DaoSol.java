package br.senai.sp.helpdesk.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.senai.sp.helpdesk.modelo.Solicitante;

public class DaoSol {
	private static int LastId;

	static {
		File arquivo = new File("sol.csv");
		if (!arquivo.exists()) {
			try {
				arquivo.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public DaoSol() throws FileNotFoundException {
		Scanner leitor = new Scanner(new FileReader("id_sol.txt"));
		LastId = leitor.nextInt();
		leitor.close();
	}

	public void inserir(Solicitante sol) throws IOException {
		sol.setId(++LastId);
		FileWriter writer = new FileWriter("sol.csv", true);
		writer.write(sol.exibir() + "\n");
		writer.close();

		writer = new FileWriter("id_sol.txt");
		writer.write(LastId + "\n");
		writer.close();
	}

	public void alterar(Solicitante solicitante) throws IOException {
		Scanner leitor = new Scanner(new FileReader("sol.csv"));
		FileWriter writer = new FileWriter("sol.new", true);
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idLinha = Integer.parseInt(dados[0]);
			if (solicitante.getId() != idLinha) {
				writer.write(linha + "\n");
			} else {
				writer.write(solicitante.exibir() + "\n");
			}
		}
		writer.close();
		leitor.close();

		File fInicial = new File("sol.csv");
		fInicial.delete();

		File f = new File("sol.new");
		f.renameTo(fInicial);
	}

	public void excluir(int id) throws IOException {
		Scanner leitor = new Scanner(new FileReader("sol.csv"));
		FileWriter writer = new FileWriter("sol.new", true);
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idLinha = Integer.parseInt(dados[0]);
			if (id != idLinha) {
				writer.write(linha + "\n");
			}
		}
		writer.close();
		leitor.close();

		File fInicial = new File("sol.csv");
		fInicial.delete();

		File f = new File("sol.new");
		f.renameTo(fInicial);
	}

	public List<Solicitante> listar() throws FileNotFoundException,
			ParseException {
		List<Solicitante> lista = new ArrayList<Solicitante>();
		Scanner leitor = new Scanner(new FileReader("sol.csv"));
		while (leitor.hasNextLine()) {
			String linha = leitor.nextLine();
			String dados[] = linha.split(";");
			Solicitante a = new Solicitante();
			a.setId(Integer.parseInt(dados[0]));
			a.setNome(dados[1]);
			a.setEmail(dados[2]);
			a.setUsuario(dados[3]);
			a.setSenha(dados[4]);
			lista.add(a);
		}
		leitor.close();
		return lista;
	}

	public List<Solicitante> buscar(String nome) throws FileNotFoundException,
			ParseException {
		List<Solicitante> lista = new ArrayList<Solicitante>();
		Scanner leitor = new Scanner(new FileReader("sol.csv"));
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			String nomeSol = dados[1];
			if (nomeSol.toLowerCase().startsWith(nome.toLowerCase())) {
				Solicitante a = new Solicitante();
				a.setId(Integer.parseInt(dados[0]));
				a.setNome(dados[1]);
				a.setEmail(dados[2]);
				a.setUsuario(dados[3]);
				a.setSenha(dados[4]);
				lista.add(a);
			}
		}
		leitor.close();
		return lista;
	}

	public Solicitante login(String usuario, String senha)
			throws FileNotFoundException, ParseException {
		Scanner leitor = new Scanner(new FileReader("sol.csv"));
		Solicitante a = null;
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			String userSol = dados[3];
			String senhaSol = dados[4];
			if (userSol.equals(usuario) && senhaSol.equals(senha)) {
				a = new Solicitante();
				a.setId(Integer.parseInt(dados[0]));
				a.setNome(dados[1]);
				a.setEmail(dados[2]);
				a.setUsuario(dados[3]);
				a.setSenha(dados[4]);
				leitor.close();
				break;
			}
		}
		leitor.close();
		return a;
	}

	public Solicitante buscar(int id) throws FileNotFoundException,
			ParseException {
		Scanner leitor = new Scanner(new FileReader("sol.csv"));
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idSol = Integer.parseInt(dados[0]);
			if (id == idSol) {
				Solicitante a = new Solicitante();
				a.setId(Integer.parseInt(dados[0]));
				a.setNome(dados[1]);
				a.setEmail(dados[2]);
				a.setUsuario(dados[3]);
				a.setSenha(dados[4]);
				leitor.close();
				return a;
			}
		}
		leitor.close();
		return null;
	}

}

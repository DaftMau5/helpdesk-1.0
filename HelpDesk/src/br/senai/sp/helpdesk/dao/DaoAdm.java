package br.senai.sp.helpdesk.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.senai.sp.helpdesk.modelo.Administrador;

public class DaoAdm {
private static int LastId;
	
	static {
		File arquivo = new File("adm.csv");
		if (!arquivo.exists()) {
			try {
				arquivo.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public DaoAdm() throws FileNotFoundException {
		Scanner leitor = new Scanner(new FileReader("id_adm.txt"));
		LastId = leitor.nextInt();
		leitor.close();
	}
	public void inserir(Administrador adm) throws IOException {
		adm.setId(++LastId);
		FileWriter writer = new FileWriter("adm.csv", true);
		writer.write(adm.exibir() + "\n");
		writer.close();

		writer = new FileWriter("id_adm.txt");
		writer.write(LastId + "\n");
		writer.close();
	}
	public void alterar(Administrador admAlterar) throws IOException {
		Scanner leitor = new Scanner(new FileReader("adm.csv"));
		FileWriter writer = new FileWriter("adm.new", true);
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idLinha = Integer.parseInt(dados[0]);
			if (admAlterar.getId() != idLinha) {
				writer.write(linha + "\n");
			} else {
				writer.write(admAlterar.exibir() + "\n");
			}
		}
		writer.close();
		leitor.close();

		File fInicial = new File("adm.csv");
		fInicial.delete();

		File f = new File("adm.new");
		f.renameTo(fInicial);
	}
	public void excluir(int id) throws IOException {
		Scanner leitor = new Scanner(new FileReader("adm.csv"));
		FileWriter writer = new FileWriter("adm.new", true);
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idLinha = Integer.parseInt(dados[0]);
			if (id != idLinha) {
				writer.write(linha + "\n");
			}
		}
		writer.close();
		leitor.close();

		File fInicial = new File("adm.csv");
		fInicial.delete();

		File f = new File("adm.new");
		f.renameTo(fInicial);
	}
	public List<Administrador> listar() throws FileNotFoundException, ParseException {
		List<Administrador> lista = new ArrayList<Administrador>();
		Scanner leitor = new Scanner(new FileReader("adm.csv"));
		while (leitor.hasNextLine()) {
			String linha = leitor.nextLine();
			String dados[] = linha.split(";");
			Administrador a = new Administrador();
			a.setId(Integer.parseInt(dados[0]));
			a.setNome(dados[1]);
			a.setEmail(dados[2]);
			a.setUsuario(dados[3]);
			a.setSenha(dados[4]);
			lista.add(a);
		}
		leitor.close();
		return lista;
	}
	public List<Administrador> buscar(String nome) throws FileNotFoundException, ParseException {
		List<Administrador> lista = new ArrayList<Administrador>();
		Scanner leitor = new Scanner(new FileReader("adm.csv"));
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			String nomeAdm = dados[1];
			if (nomeAdm.toLowerCase().startsWith(nome.toLowerCase())) {
				Administrador a = new Administrador();
				a.setId(Integer.parseInt(dados[0]));
				a.setNome(dados[1]);
				a.setEmail(dados[2]);
				a.setUsuario(dados[3]);
				a.setSenha(dados[4]);
				lista.add(a);
			}
		}
		leitor.close();
		return lista;
	}
	public Administrador buscar(int id) throws FileNotFoundException, ParseException {
		Scanner leitor = new Scanner(new FileReader("adm.csv"));
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idAdm = Integer.parseInt(dados[0]);
			if (id == idAdm) {
				Administrador a = new Administrador();
				a.setId(Integer.parseInt(dados[0]));
				a.setNome(dados[1]);
				a.setEmail(dados[2]);
				a.setUsuario(dados[3]);
				a.setSenha(dados[4]);
				leitor.close();
				return a;
			}
		}
		leitor.close();
		return null;
	}
	public Administrador login(String usuario, String senha)
			throws FileNotFoundException, ParseException {
		Scanner leitor = new Scanner(new FileReader("adm.csv"));
		Administrador a = null;
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			String userSol = dados[3];
			String senhaSol = dados[4];
			if (userSol.equals(usuario) && senhaSol.equals(senha)) {
				a = new Administrador();
				a.setNome(dados[1]);
				a.setEmail(dados[2]);
				a.setUsuario(dados[3]);
				a.setSenha(dados[4]);
				leitor.close();
				break;
			}
		}
		leitor.close();
		return a;
	}
	
}

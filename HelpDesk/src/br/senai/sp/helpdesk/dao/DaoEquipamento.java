package br.senai.sp.helpdesk.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.senai.sp.helpdesk.modelo.Equipamento;
import br.senai.sp.helpdesk.modelo.Tipo;

public class DaoEquipamento {
	private static int LastId;

	static {
		File arquivo = new File("equipamento.csv");
		if (!arquivo.exists()) {
			try {
				arquivo.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public DaoEquipamento() throws FileNotFoundException {
		Scanner leitor = new Scanner(new FileReader("id_equipamento.txt"));
		LastId = leitor.nextInt();
		leitor.close();
	}

	public void inserir(Equipamento equipamento) throws IOException {
		equipamento.setId(++LastId);
		FileWriter writer = new FileWriter("equipamento.csv", true);
		writer.write(equipamento.exibir() + "\n");
		writer.close();

		writer = new FileWriter("id_equipamento.txt");
		writer.write(LastId + "\n");
		writer.close();
	}
	public void alterar(Equipamento equipamentoAlterar) throws IOException {
		Scanner leitor = new Scanner(new FileReader("equipamento.csv"));
		FileWriter writer = new FileWriter("equipamento.new", true);
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idLinha = Integer.parseInt(dados[0]);
			if (equipamentoAlterar.getId() != idLinha) {
				writer.write(linha + "\n");
			} else {
				writer.write(equipamentoAlterar.exibir() + "\n");
			}

		}
		writer.close();
		leitor.close();

		File fInicial = new File("equipamento.csv");
		fInicial.delete();

		File f = new File("equipamento.new");
		f.renameTo(fInicial);
	}

	public void excluir(int id) throws IOException {
		Scanner leitor = new Scanner(new FileReader("equipamento.csv"));
		FileWriter writer = new FileWriter("equipamento.new", true);
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idLinha = Integer.parseInt(dados[0]);
			if (id != idLinha) {
				writer.write(linha + "\n");
			}
		}
		writer.close();
		leitor.close();

		File fInicial = new File("equipamento.csv");
		fInicial.delete();

		File f = new File("equipamento.new");
		f.renameTo(fInicial);
	}

	public List<Equipamento> listar() throws FileNotFoundException,
			ParseException {
		List<Equipamento> lista = new ArrayList<Equipamento>();
		Scanner leitor = new Scanner(new FileReader("equipamento.csv"));
		while (leitor.hasNextLine()) {
			String linha = leitor.nextLine();
			String dados[] = linha.split(";");
			Equipamento a = new Equipamento();
			a.setId(Integer.parseInt(dados[0]));
			a.setNome(dados[1]);
			a.setTipo(Tipo.values()[Integer.parseInt(dados[2])]);
			lista.add(a);
		}
		leitor.close();
		return lista;
	}

	public List<Equipamento> buscar(String nome) throws FileNotFoundException,
			ParseException {
		List<Equipamento> lista = new ArrayList<Equipamento>();
		Scanner leitor = new Scanner(new FileReader("equipamento.csv"));
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			String nomeEquipamento = dados[1];
			if (nomeEquipamento.toLowerCase().startsWith(nome.toLowerCase())) {
				Equipamento a = new Equipamento();
				a.setId(Integer.parseInt(dados[0]));
				a.setNome(dados[1]);
				a.setTipo(Tipo.values()[Integer.parseInt(dados[2])]);
				lista.add(a);
			}
		}
		leitor.close();
		return lista;
	}

	public Equipamento buscar(int id) throws FileNotFoundException,
			ParseException {
		Scanner leitor = new Scanner(new FileReader("equipamento.csv"));
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idEquipamento = Integer.parseInt(dados[0]);
			if (id == idEquipamento) {
				Equipamento a = new Equipamento();
				a.setId(Integer.parseInt(dados[0]));
				a.setNome(dados[1]);
				a.setTipo(Tipo.values()[Integer.parseInt(dados[2])]);
				leitor.close();
				return a;
			}
		}
		leitor.close();
		return null;
	}

}


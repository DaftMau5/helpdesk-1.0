package br.senai.sp.helpdesk.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import br.senai.sp.helpdesk.modelo.Chamado;
import br.senai.sp.helpdesk.modelo.Equipamento;
import br.senai.sp.helpdesk.modelo.Problema;
import br.senai.sp.helpdesk.modelo.Solicitante;
import br.senai.sp.helpdesk.modelo.Tecnico;

public class DaoChamado {
	private static int LastId;

	static {
		File arquivo = new File("chamado.csv");
		if (!arquivo.exists()) {
			try {
				arquivo.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public DaoChamado() throws FileNotFoundException {
		Scanner leitor = new Scanner(new FileReader("id_chamado.txt"));
		LastId = leitor.nextInt();
		leitor.close();
	}

	public void inserir(Chamado chamado) throws IOException {
		chamado.setId(++LastId);
		FileWriter writer = new FileWriter("chamado.csv", true);
		writer.write(chamado.exibir() + "\n");
		writer.close();

		writer = new FileWriter("id_chamado.txt");
		writer.write(LastId + "\n");
		writer.close();
	}

	public void alterar(Chamado chamadoAlterar) throws IOException {
		Scanner leitor = new Scanner(new FileReader("chamado.csv"));
		FileWriter writer = new FileWriter("chamado.new", true);
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idLinha = Integer.parseInt(dados[0]);
			if (chamadoAlterar.getId() != idLinha) {
				writer.write(linha + "\n");
			} else {
				writer.write(chamadoAlterar.exibir() + "\n");
			}
		}
		writer.close();
		leitor.close();

		File fInicial = new File("chamado.csv");
		fInicial.delete();

		File f = new File("chamado.new");
		f.renameTo(fInicial);
	}

	public void excluir(int id) throws IOException {
		Scanner leitor = new Scanner(new FileReader("chamado.csv"));
		FileWriter writer = new FileWriter("chamado.new", true);
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idLinha = Integer.parseInt(dados[0]);
			if (id != idLinha) {
				writer.write(linha + "\n");
			}
		}
		writer.close();
		leitor.close();

		File fInicial = new File("chamado.csv");
		fInicial.delete();

		File f = new File("chamado.new");
		f.renameTo(fInicial);
	}

	public List<Chamado> listar() throws FileNotFoundException, ParseException {
		List<Equipamento> equipamentos = new DaoEquipamento().listar();
		List<Solicitante> solicitantes = new DaoSol().listar();
		List<Problema> problemas = new DaoProblema().listar();
		List<Tecnico> tecnicos = new DaoTec().listar();
		List<Chamado> lista = new ArrayList<Chamado>();
		Scanner leitor = new Scanner(new FileReader("chamado.csv"));
		while (leitor.hasNextLine()) {
			String linha = leitor.nextLine();
			String dados[] = linha.split(";");
			Chamado a = new Chamado();
			a.setId(Integer.parseInt(dados[0]));
			Equipamento equipamento = new Equipamento();
			equipamento.setId(Integer.parseInt(dados[1]));
			equipamento = equipamentos.get(equipamentos.indexOf(equipamento));
			a.setEquipamento(equipamento);
			Problema problema = new Problema();
			problema.setId(Integer.parseInt(dados[2]));
			problema = problemas.get(problemas.indexOf(problema));
			a.setProblema(problema);		
			a.setLocalizacao(dados[3]);
			a.setDescricao(dados[4]);
			SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
			Calendar data = Calendar.getInstance();
			Date dataChamado = formatador.parse(dados[5]);
			data.setTime(dataChamado);
			a.setData(data);
			if (Integer.parseInt(dados[6]) !=0) {
				Tecnico tecnico = new Tecnico();
				tecnico.setId(Integer.parseInt(dados[6]));
				tecnico = tecnicos.get(tecnicos.indexOf(tecnico));
				a.setTecnico(tecnico);
			}
			a.setStatus(Boolean.parseBoolean(dados[7]));
			Solicitante solicitante = new Solicitante();
			solicitante.setId(Integer.parseInt(dados[8]));
			solicitante = solicitantes.get(solicitantes.indexOf(solicitante));
			a.setSolicitante(solicitante);
			lista.add(a);
		}
		leitor.close();
		return lista;
	}
	public List<Chamado> listarUser(Solicitante s) throws FileNotFoundException, ParseException {
		List<Equipamento> equipamentos = new DaoEquipamento().listar();
		List<Solicitante> solicitantes = new DaoSol().listar();
		List<Problema> problemas = new DaoProblema().listar();
		List<Tecnico> tecnicos = new DaoTec().listar();
		List<Chamado> lista = new ArrayList<Chamado>();
		Scanner leitor = new Scanner(new FileReader("chamado.csv"));
		while (leitor.hasNextLine()) {
			String linha = leitor.nextLine();
			String dados[] = linha.split(";");
			int user = (Integer.parseInt(dados[8]));
			Chamado a = null;
			if (s.getId() == user) {
				a = new Chamado();
				a.setId(Integer.parseInt(dados[0]));
				Equipamento equipamento = new Equipamento();
				equipamento.setId(Integer.parseInt(dados[1]));
				equipamento = equipamentos.get(equipamentos.indexOf(equipamento));
				a.setEquipamento(equipamento);
				Problema problema = new Problema();
				problema.setId(Integer.parseInt(dados[2]));
				problema = problemas.get(problemas.indexOf(problema));
				a.setProblema(problema);		
				a.setLocalizacao(dados[3]);
				a.setDescricao(dados[4]);
				SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
				Calendar data = Calendar.getInstance();
				Date dataChamado = formatador.parse(dados[5]);
				data.setTime(dataChamado);
				a.setData(data);
				if (Integer.parseInt(dados[6]) !=0) {
					Tecnico tecnico = new Tecnico();
					tecnico.setId(Integer.parseInt(dados[6]));
					tecnico = tecnicos.get(tecnicos.indexOf(tecnico));
					a.setTecnico(tecnico);
				}
				a.setStatus(Boolean.parseBoolean(dados[7]));
				Solicitante solicitante = new Solicitante();
				solicitante.setId(Integer.parseInt(dados[8]));
				solicitante = solicitantes.get(solicitantes.indexOf(solicitante));
				a.setSolicitante(solicitante);
				lista.add(a);
			}	
		}
		leitor.close();
		return lista;
	}
	public List<Chamado> listarTec(Tecnico s) throws FileNotFoundException, ParseException {
		List<Equipamento> equipamentos = new DaoEquipamento().listar();
		List<Solicitante> solicitantes = new DaoSol().listar();
		List<Problema> problemas = new DaoProblema().listar();
		List<Tecnico> tecnicos = new DaoTec().listar();
		List<Chamado> lista = new ArrayList<Chamado>();
		Scanner leitor = new Scanner(new FileReader("chamado.csv"));
		while (leitor.hasNextLine()) {
			String linha = leitor.nextLine();
			String dados[] = linha.split(";");
			int user = (Integer.parseInt(dados[6]));
			Chamado a = null;
			if (s.getId() == user) {
				a = new Chamado();
				a.setId(Integer.parseInt(dados[0]));
				Equipamento equipamento = new Equipamento();
				equipamento.setId(Integer.parseInt(dados[1]));
				equipamento = equipamentos.get(equipamentos.indexOf(equipamento));
				a.setEquipamento(equipamento);
				Problema problema = new Problema();
				problema.setId(Integer.parseInt(dados[2]));
				problema = problemas.get(problemas.indexOf(problema));
				a.setProblema(problema);		
				a.setLocalizacao(dados[3]);
				a.setDescricao(dados[4]);
				SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
				Calendar data = Calendar.getInstance();
				Date dataChamado = formatador.parse(dados[5]);
				data.setTime(dataChamado);
				a.setData(data);
				if (Integer.parseInt(dados[6]) !=0) {
					Tecnico tecnico = new Tecnico();
					tecnico.setId(Integer.parseInt(dados[6]));
					tecnico = tecnicos.get(tecnicos.indexOf(tecnico));
					a.setTecnico(tecnico);
				}
				a.setStatus(Boolean.parseBoolean(dados[7]));
				Solicitante solicitante = new Solicitante();
				solicitante.setId(Integer.parseInt(dados[8]));
				solicitante = solicitantes.get(solicitantes.indexOf(solicitante));
				a.setSolicitante(solicitante);
				lista.add(a);
			}
		}
		leitor.close();
		return lista;
	}

	// public List<Chamado> buscar(String nome) throws FileNotFoundException,
	// ParseException {
	// List<Chamado> lista = new ArrayList<Chamado>();
	// Scanner leitor = new Scanner(new FileReader("chamado.csv"));
	// while (leitor.hasNext()) {
	// String linha = leitor.nextLine();
	// String[] dados = linha.split(";");
	// String nomeChamado = dados[1];
	// if (nomeTec.toLowerCase().startsWith(nome.toLowerCase())) {
	// Tecnico a = new Tecnico();
	// a.setId(Integer.parseInt(dados[0]));
	// a.setNome(dados[1]);
	// a.setEmail(dados[3]);
	// a.setTipo(Tipo.values()[Integer.parseInt(dados[2])]);
	// lista.add(a);
	// }
	// }
	// leitor.close();
	// return lista;
	// }

	public Chamado buscar(int id) throws FileNotFoundException, ParseException {
		List<Equipamento> equipamentos = new DaoEquipamento().listar();
		List<Solicitante> solicitantes = new DaoSol().listar();
		List<Problema> problemas = new DaoProblema().listar();
		List<Tecnico> tecnicos = new DaoTec().listar();
		Scanner leitor = new Scanner(new FileReader("chamado.csv"));
		while (leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dados = linha.split(";");
			int idChamado = Integer.parseInt(dados[0]);
			if (id == idChamado) {
				Chamado a = new Chamado();
				a.setId(Integer.parseInt(dados[0]));
				Equipamento equipamento = new Equipamento();
				equipamento.setId(Integer.parseInt(dados[1]));
				equipamento = equipamentos.get(equipamentos.indexOf(equipamento));
				a.setEquipamento(equipamento);
				Problema problema = new Problema();
				problema.setId(Integer.parseInt(dados[2]));
				problema = problemas.get(problemas.indexOf(problema));
				a.setProblema(problema);		
				a.setLocalizacao(dados[3]);
				a.setDescricao(dados[4]);
				SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
				Calendar data = Calendar.getInstance();
				Date dataChamado = formatador.parse(dados[5]);
				data.setTime(dataChamado);
				a.setData(data);
				if (Integer.parseInt(dados[6]) !=0) {
					Tecnico tecnico = new Tecnico();
					tecnico.setId(Integer.parseInt(dados[6]));
					tecnico = tecnicos.get(tecnicos.indexOf(tecnico));
					a.setTecnico(tecnico);
				}
				a.setStatus(Boolean.parseBoolean(dados[7]));
				if (Integer.parseInt(dados[7]) !=0) {
					Solicitante solicitante = new Solicitante();
					solicitante.setId(Integer.parseInt(dados[6]));
					solicitante = solicitantes.get(solicitantes.indexOf(solicitante));
					a.setSolicitante(solicitante);
				}
				leitor.close();
				return a;
			}
		}
		leitor.close();
		return null;
	}
}

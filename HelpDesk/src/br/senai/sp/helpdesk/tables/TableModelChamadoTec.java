package br.senai.sp.helpdesk.tables;

import java.text.DateFormat;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.senai.sp.helpdesk.modelo.Chamado;
import br.senai.sp.helpdesk.modelo.Equipamento;
import br.senai.sp.helpdesk.modelo.Problema;
import br.senai.sp.helpdesk.modelo.Solicitante;

public class TableModelChamadoTec extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3693195881951481516L;
	List<Chamado> chamados;
	final String[] COLUNAS = { "Codigo", "Equipamento", "Data", "T�cnico",
			"Status", "Problema", "Localiza��o", "Descri��o", "Solicitante"};

	public TableModelChamadoTec(List<Chamado> chamados) {
		this.chamados = chamados;
	}

	@Override
	public int getRowCount() {
		return chamados.size();
	}

	@Override
	public int getColumnCount() {
		return COLUNAS.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Chamado a = chamados.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return a.getId();
		case 1:
			return a.getEquipamento();
		case 2:
			return DateFormat.getDateInstance(DateFormat.SHORT).format(
					a.getData().getTime());
		case 3:
			return a.getTecnico();
		case 4:
			return a.isStatus();
		case 5:
			return a.getProblema();
		case 6:
			return a.getLocalizacao();
		case 7:
			return a.getDescricao();
		case 8:
			return a.getSolicitante();
		default:
			throw new RuntimeException("Coluna Inexistente");
		}
	}

	@Override
	public String getColumnName(int column) {
		return COLUNAS[column];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return Integer.class;
		case 1:
			return Equipamento.class;
		case 2:
			return String.class;
		case 3:
			return Problema.class;
		case 4:
			return Boolean.class;
		case 5:
			return String.class;
		case 6:
			return String.class;
		case 7:
			return String.class;
		case 8:
			return Solicitante.class;
		default:
			throw new RuntimeException("Coluna Inexistente");
		}
	}

	public Chamado getChamado(int posicao) {
		return chamados.get(posicao);
	}
}

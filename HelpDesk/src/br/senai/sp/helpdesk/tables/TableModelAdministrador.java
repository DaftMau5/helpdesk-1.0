package br.senai.sp.helpdesk.tables;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.senai.sp.helpdesk.modelo.Administrador;

public class TableModelAdministrador extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Administrador> administradores;
	final String[] COLUNAS = {"Codigo", "Nome", "E-mail", "Usuario"};

	public TableModelAdministrador(List<Administrador> administradores) {
		this.administradores = administradores;
	}

	@Override
	public int getRowCount() {
		return administradores.size();
	}

	@Override
	public int getColumnCount() {
		return COLUNAS.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Administrador a = administradores.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return a.getId();
		case 1:
			return a.getNome();
		case 2:
			return a.getEmail();
		case 3:
			return a.getUsuario();
		default:
			throw new RuntimeException("Coluna Inexistente");
		}
	}

	@Override
	public String getColumnName(int column) {
		return COLUNAS[column];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return Integer.class;
		case 1:
			return String.class;
		case 2:
			return String.class;
		case 3:
			return String.class;
		default:
			throw new RuntimeException("Coluna Inexistente");
		}
	}

	public Administrador getAdministrador(int posicao) {
		return administradores.get(posicao);
	}
}

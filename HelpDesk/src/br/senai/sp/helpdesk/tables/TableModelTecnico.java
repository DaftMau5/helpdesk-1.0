package br.senai.sp.helpdesk.tables;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.senai.sp.helpdesk.modelo.Tecnico;
import br.senai.sp.helpdesk.modelo.Tipo;

public class TableModelTecnico extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8573261086579754022L;
	List<Tecnico> tecnicos;
	final String[] COLUNAS = {"Codigo", "Nome", "Especialização"};

	public TableModelTecnico(List<Tecnico> tecnicos) {
		this.tecnicos = tecnicos;
	}

	@Override
	public int getRowCount() {
		return tecnicos.size();
	}

	@Override
	public int getColumnCount() {
		return COLUNAS.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Tecnico a = tecnicos.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return a.getId();
		case 1:
			return a.getNome();
		case 2:
			return a.getTipo();
		default:
			throw new RuntimeException("Coluna Inexistente");
		}
	}

	@Override
	public String getColumnName(int column) {
		return COLUNAS[column];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return Integer.class;
		case 1:
			return String.class;
		case 2:
			return Tipo.class;
		default:
			throw new RuntimeException("Coluna Inexistente");
		}
	}

	public Tecnico getTecnico(int posicao) {
		return tecnicos.get(posicao);
	}
}

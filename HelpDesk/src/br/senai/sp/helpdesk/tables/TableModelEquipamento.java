package br.senai.sp.helpdesk.tables;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.senai.sp.helpdesk.modelo.Equipamento;
import br.senai.sp.helpdesk.modelo.Tipo;

public class TableModelEquipamento extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2965130435470116894L;
	List<Equipamento> equipamentos;
	final String[] COLUNAS = {"Codigo", "Nome", "Tipo"};

	public TableModelEquipamento(List<Equipamento> equipamentos) {
		this.equipamentos = equipamentos;
	}

	@Override
	public int getRowCount() {
		return equipamentos.size();
	}

	@Override
	public int getColumnCount() {
		return COLUNAS.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Equipamento a = equipamentos.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return a.getId();
		case 1:
			return a.getNome();
		case 2:
			return a.getTipo();
		default:
			throw new RuntimeException("Coluna Inexistente");
		}
	}

	@Override
	public String getColumnName(int column) {
		return COLUNAS[column];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return Integer.class;
		case 1:
			return String.class;
		case 2:
			return Tipo.class;
		default:
			throw new RuntimeException("Coluna Inexistente");
		}
	}

	public Equipamento getEquipamento(int posicao) {
		return equipamentos.get(posicao);
	}
}

package br.senai.sp.helpdesk.tables;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.senai.sp.helpdesk.modelo.Solicitante;

public class TableModelSolicitante extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5359496633594863555L;
	List<Solicitante> solicitantes;
	final String[] COLUNAS = {"Codigo", "Nome", "E-mail", "Usuario"};

	public TableModelSolicitante(List<Solicitante> solicitantes) {
		this.solicitantes = solicitantes;
	}

	@Override
	public int getRowCount() {
		return solicitantes.size();
	}

	@Override
	public int getColumnCount() {
		return COLUNAS.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Solicitante a = solicitantes.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return a.getId();
		case 1:
			return a.getNome();
		case 2:
			return a.getEmail();
		case 3:
			return a.getUsuario();
		default:
			throw new RuntimeException("Coluna Inexistente");
		}
	}

	@Override
	public String getColumnName(int column) {
		return COLUNAS[column];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return Integer.class;
		case 1:
			return String.class;
		case 2:
			return String.class;
		case 3:
			return String.class;
		default:
			throw new RuntimeException("Coluna Inexistente");
		}
	}

	public Solicitante getSolicitante(int posicao) {
		return solicitantes.get(posicao);
	}
}

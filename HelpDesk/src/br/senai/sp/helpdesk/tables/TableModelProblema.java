package br.senai.sp.helpdesk.tables;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.senai.sp.helpdesk.modelo.Problema;

public class TableModelProblema extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3898116907922364831L;
	List<Problema> problema;
	final String[] COLUNAS = {"Codigo", "Nome"};

	public TableModelProblema(List<Problema> problema) {
		this.problema = problema;
	}

	@Override
	public int getRowCount() {
		return problema.size();
	}

	@Override
	public int getColumnCount() {
		return COLUNAS.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Problema a = problema.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return a.getId();
		case 1:
			return a.getNome();
		default:
			throw new RuntimeException("Coluna Inexistente");
		}
	}

	@Override
	public String getColumnName(int column) {
		return COLUNAS[column];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return Integer.class;
		case 1:
			return String.class;
		default:
			throw new RuntimeException("Coluna Inexistente");
		}
	}

	public Problema getProblema(int posicao) {
		return problema.get(posicao);
	}
}

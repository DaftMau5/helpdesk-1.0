package br.senai.sp.helpdesk.document;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class DescricaoDocument extends PlainDocument {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
		if (getLength() + str.length() <= 250) {
			super.insertString(offs, str.replace(";", ""), a);
		}
	}
}
